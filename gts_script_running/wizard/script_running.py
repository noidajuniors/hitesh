from odoo import api, models, fields, _
from odoo.exceptions import UserError
import xlrd
import math
import base64
import logging
from io import BytesIO
from odoo.tools import pycompat
import string
from datetime import datetime
from odoo.tools.misc import DEFAULT_SERVER_DATE_FORMAT

logger = logging.getLogger('Import')

logger = logging.getLogger('Script')


class script_running(models.TransientModel):
    ''' Used for Script Running '''
    _name = 'script.running'

    sheet = fields.Binary('Sheet')
    branch_id = fields.Many2one('res.branch', 'Branch')
    date = fields.Date('Date')
    e_date = fields.Date('End Date')
    seq_prefix = fields.Char('Prifix')
    seq_start = fields.Char('Start seq')
    xlsx_file = fields.Binary("Upload File")
    filename = fields.Char('Filename')

    @api.multi
    def update_image_in_product(self):
        ''' Update Images in Product '''
        import urllib.request
        from PIL import Image
        import io
        from zipfile import ZipFile
        import os
        import requests
        if not self.xlsx_file:
            raise UserError(_('Select XLS file !'))
        file = base64.decodebytes(self.xlsx_file)
        product_obj = self.env['product.template']
        product_image_obj = self.env['product.image']
        file_path = '/tmp/products.xlsx'
        fp = open(file_path, 'wb')
        fp.write(file)
        fp.close()
        book = xlrd.open_workbook(file_path)
        sh = book.sheet_by_index(0)
        logger.info('row count : %d', sh.nrows)
        sku, product_name, all_images, image1, image2, image3 = 0, 1, 2, 3, 4, 5
        for line in range(1, sh.nrows):
            row = sh.row_values(line)
            logger.info(' line.%s.row %s', line, row)
            if not row[sku]:
                continue
            product = product_obj.search([('barcode', '=', str(row[sku]).strip())], limit=1)
            if not product:
                continue
                # raise UserError(_('Product not found %s !') % (row[sku]))
            if row[image1]:
                try:
                    image_url = str(row[image1]).strip().split('?')[0] + '?dl=1'
                    image = Image.open(urllib.request.urlopen(image_url))
                    # print('image...', type(image))
                    buf = BytesIO()
                    # Save the image as jpeg to the buffer
                    image.save(buf, 'jpeg')
                    # Rewind the buffer's file pointer
                    buf.seek(0)
                    # Read the bytes from the buffer
                    image_bytes = buf.read()
                    buf.close()
                    image_byte = base64.b64encode(image_bytes)
                    product.image = image_byte
                except Exception as e:
                    logger.error('error %s', e.args)
            # image_zip = urllib.request.urlopen(image_dir_url)
            # print('image_zip..', image_zip, dir(image_zip))
            # print('image_zip...', image_zip.read())
            # with open('/tmp/' + 'zip1' + '.zip', 'wb') as f:
            #     f.write(image_zip.read())
            #     f.close()
            # url = 'https://publib.boulder.ibm.com/bpcsamp/v6r1/monitoring/clipsAndTacks/download/ClipsAndTacksF1.zip'
            # Download Zip files for directory
            if str(row[all_images]).strip():
                try:
                    image_dir_url = str(row[all_images]).strip().split('?')[0] + '?dl=1'
                    r = requests.get(image_dir_url)
                    zip_file_path = '/tmp/zip' + str(line)
                    with open(zip_file_path + '.zip', "wb") as zip_file:
                        zip_file.write(r.content)
                    # Reading Zip File
                    with ZipFile(zip_file_path + '.zip', 'r') as zip_ref:
                        zip_ref.extractall(zip_file_path)
                    # Iterating through extracted zip files and
                    for subdir, dirs, files in os.walk(zip_file_path):
                        for filename in files:
                            image_file_path = subdir + os.sep + filename
                            if image_file_path.endswith(".jpg") or image_file_path.endswith(
                                    ".jpeg") or image_file_path.endswith(".png"):
                                im = Image.open(image_file_path)
                                buf = BytesIO()
                                # Save the image as jpeg to the buffer
                                im.save(buf, 'jpeg')
                                # Rewind the buffer's file pointer
                                buf.seek(0)
                                # Read the bytes from the buffer
                                image_bytes = buf.read()
                                buf.close()
                                image_byte = base64.b64encode(image_bytes)
                                product_image_obj.create({
                                    'name': filename,
                                    'image': image_byte,
                                    'product_tmpl_id': product.id
                                })
                except Exception as e:
                    logger.error('error %s', e.args)
            # if line > 0:
            #     break
        # aa
        return True

    @api.multi
    def account_entry_update(self):
        inv_ids = self.env['account.invoice'].search([
            ('type', '=', 'in_invoice'), ('branch_id', '=', self.branch_id.id),
            ('date_invoice', '>=', self.date)
        ], order='date_invoice asc')
        if inv_ids:
            x = int(self.seq_start)
            for i in inv_ids:
                i.move_id.name = x
                x += 1

    @api.multi
    def account_entry_add_name(self):
        inv_ids = self.env['account.invoice'].search([
            ('type', '=', 'in_invoice'), ('branch_id', '=', self.branch_id.id),
            ('date_invoice', '>=', self.date), ('date_invoice', '<=', self.e_date)
        ], order='date_invoice asc')
        if inv_ids:
            x = int(self.seq_start)
            for i in inv_ids:
                i.move_id.name = self.seq_prefix + str(x)
                i.move_name = self.seq_prefix + str(x)
                x += 1

    @api.multi
    def update_entry(self):
        ''' method for create move line and move from assest'''
        assets = self.env['account.asset.asset']
        account_move = self.env['account.move']
        move_line = self.env['account.move.line']
        account = self.env['account.account']
        journal = self.env['account.journal']
        fixed_account_id = account.search([('name', '=', '044002 Fixed Assets')], limit=1)
        cash_account_id = account.search([('name', '=', 'Cash')], limit=1)
        dep_account_id = account.search([('name', '=', 'Depreciation')], limit=1)
        journal_id = journal.search([('name', '=', 'Asset Journal')], limit=1)

        ml_list1 = []
        ml_lst2 = []
        assets_id = assets.search([('date', '=', '2018-03-31')])

        for asset in assets_id:
            if asset.purchase_cost > 0.0:
                ml_list1.append((0, 0, {
                    'account_id': asset.category_id.account_asset_id.id,
                    'branch_id': asset.branch_id.id,
                    'name': 'opening assets',
                    'debit': asset.purchase_cost
                }))
                ml_list1.append((0, 0, {
                    'account_id': cash_account_id.id,
                    'branch_id': asset.branch_id.id,
                    'name': 'opening assets',
                    'credit': asset.purchase_cost
                }))
                move_id = account_move.create({'date': asset.date,
                                               'branch_id': asset.branch_id.id,
                                               'journal_id': journal_id.id,
                                               'ref': asset.name,
                                               'line_ids': ml_list1,
                                               'asset_id': asset.id,
                                               })
                move_id.post()
                ml_list1 = []
            if asset.deprecation > 0.0:
                ml_lst2.append((0, 0, {
                    'account_id': asset.category_id.account_asset_id.id,
                    'branch_id': asset.branch_id.id,
                    'name': 'opening assets',
                    'credit': asset.deprecation
                }))
                ml_lst2.append((0, 0, {
                    'account_id': asset.category_id.account_depreciation_expense_id.id,
                    'branch_id': asset.branch_id.id,
                    'name': 'opening assets',
                    'debit': asset.deprecation
                }))
                move_id = account_move.create({'date': asset.date,
                                               'branch_id': asset.branch_id.id,
                                               'journal_id': journal_id.id,
                                               'ref': asset.name,
                                               'asset_id': asset.id,
                                               'line_ids': ml_lst2})
                move_id.post()
                ml_lst2 = []

    @api.multi
    def import_asset(self):
        asset = self.env['account.asset.asset']
        category = self.env['account.asset.category']
        branch = self.env['res.branch']
        if not self.sheet:
            raise UserError(_('Please add file'))
        val = base64.decodestring(self.sheet)
        fp = BytesIO()
        fp.write(val)
        # f = open("test lot.txt", "w+")
        book = xlrd.open_workbook(file_contents=fp.getvalue())
        sh = book.sheet_by_index(0)
        count = 0
        logger.info('row count : %d', sh.nrows)
        asset.unlink()
        for line in range(1, sh.nrows):  # sh.nrows
            count = count + 1
            row = sh.row_values(line)
            mrp = 0.0
            p_date = isinstance(row[2], float)
            imp_date = isinstance(row[3], float)
            sys_date = isinstance(row[1], float)
            if p_date == True:
                seconds = (row[2] - 25569) * 86400.0
                date = datetime.utcfromtimestamp(seconds)
                purchase_date = date.strftime('%Y/%m/%d')
            if imp_date == True:
                seconds = (row[3] - 25569) * 86400.0
                date = datetime.utcfromtimestamp(seconds)
                import_date = date.strftime('%Y/%m/%d')
            if sys_date == True:
                seconds = (row[1] - 25569) * 86400.0
                date = datetime.utcfromtimestamp(seconds)
                s_date = date.strftime('%Y/%m/%d')
            branch_id = branch.search([('name', '=', row[0])])
            categ_id = category.search([('name', '=', row[5])])
            importdate = datetime.strptime(import_date, "%Y/%m/%d")
            p_date = datetime.strptime(purchase_date, "%Y/%m/%d")

            if (importdate.year - p_date.year) > 0:
                method_num = abs(categ_id.method_number - abs((importdate.year - p_date.year)))
            else:
                method_num = categ_id.method_number

            asset_id = asset.create({'name': row[4],
                                     'branch_id': branch_id.id,
                                     'category_id': categ_id.id,
                                     'purchase_date': purchase_date,
                                     'import_date': import_date,
                                     'value': row[8],
                                     'salvage_value': int(row[9]),
                                     'deprecation': row[7],
                                     'purchase_cost': row[6],
                                     'date': s_date,

                                     })
            asset_id._amount_residual()
            asset_id.onchange_method_number()

        return True

    @api.multi
    def receive_product(self):
        asset_ids = self.env['account.asset.asset'].search([])
        for res in asset_ids:
            asset_line = res.depreciation_line_ids
            if asset_line:
                for rec in asset_line:
                    if rec.depreciation_date_to == '2019-03-31' and not rec.move_id:
                        rec.create_move()
            res.validate()
        # moves_ids = self.env['account.move.line'].search([('product_id', '!=', False),
        #                                           ('debit', '>', 0.0), ('date', '>=', '2018-04-01'),
        #                                           ('journal_id', '!=', 1), ('account_id', '!=',32)])
        # count = 0
        # print(moves_ids)
        # for res in moves_ids:
        #     if res.product_id.categ_id.id == 2378:
        #         count += 1
        #         print(res, count, res.account_id)
        #         res.update({'account_id': res.product_id.categ_id.property_account_expense_categ_id.id})

        # moves = self.env['stock.move'].search([('purchase_line_id', '!=', False)])
        # count = 0
        # for data in moves:
        #     count +=1
        #     purchase_line_ids = self.env['purchase.order.line'].search([('id', '=', data.id)])
        #     line_count = 0
        #     for line in purchase_line_ids:
        #         line_count += 1
        #         data.purchase_price = line.price_unit
        #         data.purchase_cost_price = line.price_unit * data.quantity_done
        #     # # print(purchase_ids)
        #     # for pur in purchase_ids:
        #     #     data.write({'purchase_price': pur.price_unit})

    @api.multi
    def confirm_assets(self):
        assets_ids = self.env['account.asset.asset'].search([('state', '=', 'draft')])
        for rec in assets_ids:
            rec.validate()


    @api.multi
    def post_assets_entry(self):
        assets_ids = self.env['account.asset.asset'].search([('state', '=', 'open')])
        for rec in assets_ids:
            for line in rec.depreciation_line_ids:
                date = datetime.strptime(line.depreciation_date, DEFAULT_SERVER_DATE_FORMAT)
                if date.year < 2021 and not line.move_id:
                    line.create_move()
                    line.move_id.post()

    @api.multi
    def done_mo(self):
        ''' method for create MO from BOM'''
        mo = self.env['mrp.production']
        count = 0
        bom_ids = self.env['mrp.bom'].search([('related_mo', '=', False),
                                              ('event_date', '<=', '2019-01-31'),
                                              ('branch_id', '=', self.branch_id.id)])
        for bom in bom_ids:
            count = count + 1
            mrp_produce = self.env['mrp.product.produce']
            # bom.button_create_production()

            bom.action_confirm()
            if not bom.product_id:
                continue
            mo_id = self.env['mrp.production'].with_context(active_ids=bom.id).create({'product_id': bom.product_id.id,
                                                                                       'client_name': bom.client_name.id,
                                                                                       'venue': bom.venue,
                                                                                       'project_managers': bom.project_managers.id,
                                                                                       'date_planned_start': fields.Datetime.now(),
                                                                                       'branch_id': bom.branch_id.id,
                                                                                       'process_stage': 'bom',
                                                                                       'bom_id': bom.id,
                                                                                       'product_uom_id': bom.product_id.uom_id.id
                                                                                       })
            if not mo_id.move_raw_ids:
                continue
            mo_id.onchange_product_id()
            context = self.env.context.copy() or {}
            context['active_id'] = mo_id.id
            context['active_model'] = 'mrp.production'
            action = mo_id.with_context(context).open_produce_product()
            action['context'] = context
            # create produce Wizard & Production as per LOT
            production = self.env['mrp.product.produce'].with_context(context).create({
                'product_id': mo_id.product_id.id,
                'product_qty': mo_id.product_qty,
            })
            # for Default functions
            production.with_context(context).default_get(
                ['product_id', 'product_qty', 'product_uom_id'])

            production.do_produce()
            mo_id.button_mark_done()
            moves = self.env['stock.move'].search([('name', '=', mo_id.name)])
            for mv in moves:
                if mo_id.expected_closing_date:
                    mv.date = mo_id.expected_closing_date

    @api.multi
    def update_late_date(self):
        mo = self.env['account.move'].search([('create_date', '>=', '2019-05-01'),
                                              ('date', '<=', '2019-04-30'), ('date', '>=', '2019-04-01')])
        for m in mo:
            m.write({'is_late_entry': True})

    @api.multi
    def update_mo_date(self):
        mo = self.env['mrp.production'].search([('state', 'in', ('done', 'progress', 'planned'))])
        for m in mo:
            if m.expected_closing_date:
                moves = self.env['stock.move'].search([('name', '=', m.name)])
                if moves:
                    for mv in moves:
                        mv.write({'date': m.expected_closing_date, 'picking_type_id': m.picking_type_id.id})

    @api.multi
    def update_po_date(self):
        purchase = self.env['purchase.order'].search([('date_order', '>=', '2019-03-01 00:00:00'),
                                                      ('date_order', '<=', '2019-03-31 23:59:59'),
                                                      ('state', '=', 'purchase')])
        for rec in purchase:
            move = self.env['stock.move'].search([('origin', '=', rec.name)])
            if move:
                for mv in move:
                    if rec.create_date:
                        mv.write({'date': rec.date_order})

    @api.multi
    def update_inventory_date(self):
        inventory = self.env['stock.inventory'].search([])
        for inv in inventory:
            moves = self.env['stock.move'].search([('inventory_id', '=', inv.id)])
            if moves:
                for mv in moves:
                    mv.write({'date': inv.date})

    @api.multi
    def update_inventory_value(self):
        inventory = self.env['stock.inventory'].search([('id', 'in', (5881, 5879, 5878))])
        for data in inventory:
            for inv_line in data.line_ids:
                inv_line.update({'total_real_cost': inv_line.product_cost * inv_line.product_qty})

    @api.multi
    def update_invoice_value(self):
        invoice_ids = self.env['account.invoice'].search([
            ('type', '=', 'out_invoice'), '|', ('date_invoice', '>', '2019-12-14'), ('date_invoice', '=', False)])
        for data in invoice_ids:
            data.total_sales_price = data.total_sales_price_new
            data.total_cost = data.total_cost_new
            # data.update({
            #     'total_sales_price': data.total_sales_price_new,
            #     'total_cost': data.total_cost_new
            # })

    @api.multi
    def create_property_fields(self):
        search_id = self.env['res.partner'].search([('customer', '=', True), ('active', '=', True)])
        for partner in search_id:
            ir_property_obj = self.env['ir.property']
            fields_obj = self.env['ir.model.fields']
            company = self.env['res.company'].search([])
            field = fields_obj.search([('name', '=', 'default_sale_tax_id'), ('model_id', '=', 77)])
            for data in company:
                if partner.country_id and partner.country_id.code == 'GB':
                    sale_tax = self.env['account.tax'].search([
                        ('company_id', '=', data.id),
                        ('tax_type', '=', 'T1'),
                        ('type_tax_use', '=', 'sale')
                    ], limit=1)
                else:
                    sale_tax = self.env['account.tax'].search([
                        ('company_id', '=', data.id),
                        ('tax_type', '=', 'T4'),
                        ('type_tax_use', '=', 'sale')
                    ], limit=1)
                ir_property_obj.create({
                    'name': 'default_sale_tax_id',
                    'fields_id': field.id,
                    'res_id': 'res.partner' + ',' + str(partner.id),
                    'type': 'many2one',
                    'value_reference': 'account.tax' + ',' + str(sale_tax.id),
                    'company_id': data.id

                })

    @api.multi
    def update_taxes_into_sol(self):
        sale_order_lines = self.env['sale.order.line'].search([])
        for line in sale_order_lines:
            if line.order_id.partner_id.parent_id:
                taxes = line.order_id.partner_id.parent_id.default_sale_tax_id and \
                        line.order_id.partner_id.parent_id.default_sale_tax_id.id
            else:
                taxes = line.order_id.partner_id.default_sale_tax_id and \
                        line.order_id.partner_id.default_sale_tax_id.id
            line.tax_id = [(6, 0, [taxes])]

    @api.multi
    def update_taxes_into_ail(self):
        account_invoice_lines = self.env['account.invoice.line'].search([
            ('invoice_id.date_invoice', '>=', '2019-10-01'), ('invoice_id.date_invoice', '<=', '2019-12-13')
        ])

        for line in account_invoice_lines:
            if line.invoice_id.partner_id.parent_id:
                taxes = line.invoice_id.partner_id.parent_id.default_sale_tax_id and \
                        line.invoice_id.partner_id.parent_id.default_sale_tax_id.id
            else:
                taxes = line.invoice_id.partner_id.default_sale_tax_id and \
                        line.invoice_id.partner_id.default_sale_tax_id.id
            line.invoice_line_tax_ids = [(6, 0, [taxes])]

    @api.multi
    def update_taxes_into_aml(self):
        account_move_lines = self.env['account.move.line'].search(
            [('date', '>', '2019-12-13'), ('tax_line_id.tax_type', '=', 'T0')])

        for line in account_move_lines:
            if line.partner_id.parent_id:
                taxes = line.partner_id.parent_id.default_sale_tax_id and \
                        line.partner_id.parent_id.default_sale_tax_id.id
                name = line.partner_id.parent_id.default_sale_tax_id and \
                       line.partner_id.parent_id.default_sale_tax_id.name
            else:
                taxes = line.partner_id.default_sale_tax_id and \
                        line.partner_id.default_sale_tax_id.id
                name = line.partner_id.default_sale_tax_id and \
                       line.partner_id.default_sale_tax_id.name
            line.tax_line_id = taxes
            line.name = name

    @api.multi
    def update_taxes_into_tax_lines(self):
        tax_lines = self.env['account.invoice.tax'].search(
            [('invoice_id.date_invoice', '>=', '2019-10-01'), ('invoice_id.date_invoice', '<=', '2019-12-13')])
        count = 0
        for line in tax_lines:
            count += 1
            if line.invoice_id.partner_id.parent_id:
                name = line.invoice_id.partner_id.parent_id.default_sale_tax_id and \
                       line.invoice_id.partner_id.parent_id.default_sale_tax_id.name
            else:
                name = line.invoice_id.partner_id.default_sale_tax_id and \
                       line.invoice_id.partner_id.default_sale_tax_id.name
            line.name = name

    @api.multi
    def update_base_price(self):
        ine_ids = self.env['product.product'].search([])
        for data in ine_ids:
            data.base_price = data.standard_price

    @api.multi
    def update_product_data(self):
        file = base64.decodebytes(self.xlsx_file)
        barcode, category, parent_category, alternative, accessory, feature = 0, 1, 2, 3, 4, 5
        file_path = '/tmp/invoice.xlsx'
        fp = open(file_path, 'wb')
        fp.write(file)
        fp.close()
        book = xlrd.open_workbook(file_path)
        sh = book.sheet_by_index(0)
        logger.info('row count : %d', sh.nrows)
        product_obj = self.env['product.product']
        category_obj = self.env['product.public.category']
        for line in range(1, sh.nrows):
            row = sh.row_values(line)
            products = product_obj.search([('barcode', '=', row[barcode])], limit=1)
            for data in products:
                data.product_tmpl_id.website_published = True
                # alternative_prod = product_obj.search([('barcode', 'in', row[alternative].split(','))])
                # alter_list = []
                # for alter in alternative_prod:
                #     alter_list.append(alter.product_tmpl_id.id)
                # accessory_prod = product_obj.search([('barcode', 'in', row[accessory].split(','))])
                # accessory_list = []
                # for acc in accessory_prod:
                #     accessory_list.append(acc.id)
                # categories = category_obj.search([('name', '=', row[category]), ('parent_id.name', '=', row[parent_category])])
                # category_list = []
                # for categ in categories:
                #     category_list.append(categ.id)
                # data.description_website = row[feature]
                # data.accessory_product_ids = [(6, 0, accessory_list)]
                # data.alternative_product_ids = [(6, 0, alter_list)]
                # data.public_categ_ids = [(6, 0, category_list)]

    @api.multi
    def update_gst_data(self):
        file = base64.decodebytes(self.xlsx_file)
        name, gstin = 0, 1
        file_path = '/tmp/invoice.xlsx'
        fp = open(file_path, 'wb')
        fp.write(file)
        fp.close()
        book = xlrd.open_workbook(file_path)
        sh = book.sheet_by_index(0)
        logger.info('row count : %d', sh.nrows)
        product_obj = self.env['res.partner']
        for line in range(1, sh.nrows):
            row = sh.row_values(line)
            customers = product_obj.search([('name', '=', row[name])], limit=1)
            for data in customers:
                if row[gstin]:
                    data.vat = row[gstin]
                    data.gstin_registered = True

    @api.multi
    # @api.depends('qty_invoiced', 'qty_delivered', 'product_uom_qty', 'order_id.state')
    def script_get_to_invoice_qty(self):
        """
        Compute the quantity to invoice. If the invoice policy is order, the quantity to invoice is
        calculated from the ordered quantity. Otherwise, the quantity delivered is used.
        """
        sale_order_line = self.env['sale.order.line'].search([])
        for line in sale_order_line:
            if line.order_id.state in ['sale', 'done'] and line.order_id.invoice_status == 'to invoice':
                if line.product_id.invoice_policy == 'order':
                    line.qty_to_invoice = line.product_uom_qty - line.qty_invoiced
                else:
                    line.qty_to_invoice = line.qty_delivered - line.qty_invoiced
            # else:
            #     line.qty_to_invoice = 0

    @api.multi
    def create_invoice_script(self):
        file = base64.decodebytes(self.xlsx_file)
        source, move_name, date_invoice = 0, 1, 2
        file_path = '/tmp/invoice.xlsx'
        fp = open(file_path, 'wb')
        fp.write(file)
        fp.close()
        book = xlrd.open_workbook(file_path)
        sh = book.sheet_by_index(0)
        logger.info('row count : %d', sh.nrows)
        sale_obj = self.env['sale.order']
        for line in range(1, sh.nrows):
            row = sh.row_values(line)
            origin = 'T-' + str(row[source])
            # origin = row[source]
            serial = row[date_invoice]
            seconds = (serial - 25569) * 86400.0
            sales = sale_obj.search([('origin', '=', origin), ('invoice_status', '=', 'to invoice')], limit=1).ids
            # sales = sale_obj.search([('origin', '=', origin)], limit=1)
            # for data in sales:
            #     print(data)
            #     data.invoice_date_temp = datetime.utcfromtimestamp(seconds).strftime('%Y-%m-%d')
            if sales:
                context = {"active_model": 'sale.order', "active_ids": sales, "open_invoices": True}
                payment = self.env['sale.advance.payment.inv'].create({'advance_payment_method': 'delivered'})
                action_invoice = payment.with_context(context).create_invoices()
                invoice_id = action_invoice['res_id']
                invoices = self.env['account.invoice'].browse(invoice_id)
                invoices.write(
                    {'date_invoice': datetime.utcfromtimestamp(seconds).strftime('%Y-%m-%d'),
                     'date': datetime.utcfromtimestamp(seconds).strftime('%Y-%m-%d'),
                     'move_name': row[move_name]
                     }
                )

    @api.multi
    def update_partner(self):
        file = base64.decodebytes(self.xlsx_file)
        payable_account, name = 0, 1
        file_path = '/tmp/invoice.xlsx'
        fp = open(file_path, 'wb')
        fp.write(file)
        fp.close()
        book = xlrd.open_workbook(file_path)
        sh = book.sheet_by_index(0)
        logger.info('row count : %d', sh.nrows)
        partner_obj = self.env['res.partner']
        account_obj = self.env['account.account']
        for line in range(1, sh.nrows):
            row = sh.row_values(line)
            partner_ids = partner_obj.search([('name', '=', row[name])], limit=1)
            account = account_obj.search([('code', '=', int(row[payable_account]))], limit=1)
            print(row)
            if partner_ids:
                for data in partner_ids:
                    data.property_account_payable_id = account.id
            else:
                dict_ = {
                    'name': row[name],
                    'country_id': 104,
                    'company_type': 'person',
                    'state_id': 597,
                    'is_company': True,
                    'property_account_payable_id': account.id
                }
                partner_obj.create(dict_)
