from odoo import http
from odoo.addons.http_routing.models.ir_http import unslug
from odoo.http import request


class WebsiteMentorshipPage(http.Controller):

    @http.route(['/mentorship/<partner_id>'], type='http', auth="public", website=True)
    def partners_detail(self, partner_id, **post):
        _, partner_id = unslug(partner_id)
        if partner_id:
            partner_sudo = request.env['mentorship.forum'].sudo().browse(partner_id)
            if partner_sudo.exists():
                values = {
                    'main_object': partner_sudo,
                    'partner': partner_sudo,
                    'edit_page': True
                }
                return request.render("gts_coe_mentor.mentorship_page", values)
        return request.not_found()

    @http.route(['/target/session/<int:send_mail_id>'], type='http', auth="public", website=True)
    def page_event_target(self, send_mail_id=None, **kw):
        event_mail_line = request.env['event.schedule.line'].browse(send_mail_id)
        return request.render('gts_coe_mentor.template_for_taget_event', {
            'line_id': event_mail_line,
            'session_id': event_mail_line.id
        })

    @http.route(['/target/session/confirm/<int:session_id>'], type='http', auth="public", website=True)
    def session_confirm(self, session_id=None, **kw):
        if session_id:
            session = request.env['event.schedule.line'].browse(session_id)
            if session:
                session.sudo().action_accept_button()
                return request.render('gts_coe_mentor.session_confirm_page', {
                    'line_id': session,
                })

    @http.route(['/target/session/reject/<int:session_id>'], type='http', auth="public", website=True)
    def session_reject(self, session_id=None, **kw):
        if session_id:
            session = request.env['event.schedule.line'].browse(session_id)
            if session:
                session.sudo().action_reject_button()
                return request.render('gts_coe_mentor.session_reject_page', {
                    'line_id': session,
                })