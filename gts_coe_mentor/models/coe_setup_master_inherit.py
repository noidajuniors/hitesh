from odoo import api, fields, models, _


class CoeSetup(models.Model):
    _inherit = 'coe.setup.master'

    @api.multi
    def action_partner_button(self):

        partner_ids = self.env['res.partner'].search([('coe_id', '=', self.id),
                                                      ('coe_partner_type','=','partner')])
        action = self.env.ref('gts_coe_mentor.action_member_partner').read()[0]
        if len(partner_ids) > 1:
            action['domain'] = [('id', 'in', partner_ids.ids)]
        elif len(partner_ids) == 1:
            action['views'] = [(self.env.ref('gts_coe_mentor.res_partner_member_partner_form').id, 'form')]
            action['res_id'] = partner_ids.ids[0]
        else:
            action['domain'] = [('id', 'in', partner_ids.ids)]
        action['context'] = {'default_coe_id': self.id,
                             'default_coe_partner_type':'partner'}
        return action



    @api.multi
    def action_member_button(self):
        partner_ids = self.env['res.partner'].search([('coe_id', '=', self.id),('coe_partner_type','=','member')])
        action = self.env.ref('gts_coe_mentor.action_member_new1').read()[0]
        if len(partner_ids) > 1:
            action['domain'] = [('id', 'in', partner_ids.ids)]
        elif len(partner_ids) == 1:
            action['views'] = [(self.env.ref('gts_coe_mentor.res_member_form').id, 'form')]
            action['res_id'] = partner_ids.ids[0]
        else:
            action['domain'] = [('id', 'in', partner_ids.ids)]
        action['context'] = {'default_coe_id': self.id,
                             'default_coe_partner_type':'member'}
        return action


    @api.multi
    def action_mentor_button(self):

        partner_ids = self.env['res.partner'].search([('coe_id', '=', self.id),('coe_partner_type','=','member')])
        action = self.env.ref('gts_coe_mentor.res_partner_action_menu_view').read()[0]
        if len(partner_ids) > 1:
            action['domain'] = [('id', 'in', partner_ids.ids)]
        elif len(partner_ids) == 1:
            action['views'] = [(self.env.ref('gts_coe_mentor.new_res_partner_form_view_mentor').id, 'form')]
            action['res_id'] = partner_ids.ids[0]
        else:
            action['domain'] = [('id', 'in', partner_ids.ids)]
        action['context'] = {'default_coe_id': self.id,
                             'default_coe_partner_type':'mentor'}
        return action

