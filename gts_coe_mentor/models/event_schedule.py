import babel.dates
import pytz
from odoo import api, fields, models
from odoo import tools
from odoo.tools.translate import _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, pycompat
from odoo.exceptions import UserError, ValidationError


class EventSchedule(models.Model):
    _name = 'event.schedule'
    _rec_name = "name"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description='Session Schedule'

    @api.model
    def default_get(self, fields):
        res = super(EventSchedule, self).default_get(fields)
        if self.env.user.partner_type == 'coe':
            coe_id = self.env['coe.setup.master'].search([('partner_id', '=', self.env.user.partner_id.id)])
            res.update({'coe_id': coe_id.id})
        return res

    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting_for_approval', 'Waiting for Approval'),
        ('approved', 'Approved'),
        ('publish','Published'),
        ('cancel', 'Cancel'),
        ('reject', 'Rejected')
    ], string='Status', copy=False, default='draft', track_visibility='onchange')
    sequance= fields.Char('Reference',copy=False, default=lambda self: _('/'), readonly=True)
    name=fields.Char("Name")
    from_date =fields.Datetime("From Date")
    to_date =fields.Datetime("To Date")
    schedule_line=fields.One2many('event.schedule.line', 'schedule_id' , copy=True ,string="Schedule")
    coe_id = fields.Many2one('coe.setup.master', string='COE')

    @api.onchange('from_date','to_date',)
    def period_oncahge_validation(self):
        for s in self:
            if s.to_date and s.from_date:
                if s.to_date < s.from_date:
                    raise ValidationError("Peroid End date Should be greater than Start Date!")

    def request_button(self):
        self.sequance = self.env['ir.sequence'].next_by_code('event.schedule.sequence') or _('')
        template_id = self.env.ref('gts_coe_mentor.email_template_event_session_request_id')
        view_id = self.env.ref('gts_coe_mentor.action_event_schedule_line_view')

        if self.schedule_line:
            for line in self.schedule_line:
                rendering_context = dict(line._context)
                rendering_context.update({
                    'action_id': self.env['ir.actions.act_window'].search([('view_id', '=', view_id.id)],limit=1).id,
                    'dbname': self._cr.dbname, })
                params = "/target/session/%s" % (line.id)
                template_id.body_html = template_id.body_html.replace('_customer_url', params)
                current_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
                customer_url = str(current_url) + str(params)

                if template_id:
                    values = template_id.with_context(rendering_context).generate_email(line.id)
                    values['email_to'] = line.mentor_id.email
                    values['email_from'] = self.env.user.partner_id.email
                    mail = self.env['mail.mail'].create(values)
                    try:
                        mail.send()
                    except Exception:
                        pass
        self.state='waiting_for_approval'

    def approve_button(self):
        not_applicable = self.schedule_line.filtered(lambda sl: sl.state in ('draft')) or None
        if not_applicable:
            for line in not_applicable:
                line.state='reject'
        self.state = 'approved'

    def reject_button(self):
        self.state='reject'

    def cancel_button(self):
        self.state='cancel'

    def publish_button_action(self):
        if self.schedule_line:
            event_list=[]
            for line in self.schedule_line:
                if line.state=='accept':
                    event_id = self.env['event.event'].create({
                        'name': line.schedule_id.name,
                        'event_type_id': line.event_type_id.id,
                        'date_begin':line.event_date_start,
                        'date_end': line.event_date_end,
                        'description':"Mentorship Session/"+ ""+line.category_type.category_name,
                        'event_schedule_id':self.id,
                        'is_normal':False,
                        'mentor_id':line.mentor_id.id,
                        'mentor_event':True,
                        'user_id':line.mentor_id.user_id.id,
                        'location_id':line.schedule_id.coe_id.stpi_location_id.id if line.schedule_id.coe_id.stpi_location_id else line.schedule_id.coe_id.centor_location_id.id,
                        'coe_id':line.schedule_id.coe_id.id, })
                    event_list.append(event_id.id)
                    event_id.button_confirm()
                    event_id._onchange_type()
        self.event_ids=[(6,0,event_list)]
        self.event_count=len(self.event_ids)
        self.state='publish'

class EventScheduleLine(models.Model):
    _name = 'event.schedule.line'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description="Event schedule Line"
    _rec_name='schedule_id'

    schedule_id=fields.Many2one('event.schedule', ondelete='cascade',)
    category_type = fields.Many2one('mentor.category', string='Type')
    mentor_id = fields.Many2one('res.partner', string='Mentor')
    domain_id=fields.Many2one('domain.master',String="Domain")
    technology_id=fields.Many2one('technology.master',String="Technology")
    venue=fields.Char("Venue")
    description = fields.Char("Description")
    start=fields.Datetime("Start")
    event_date_start=fields.Datetime("Session Start")
    event_date_end=fields.Datetime("Session End")
    event_type_id = fields.Many2one('event.type', string='Session Type',ondelete='cascade')
    event_mode = fields.Selection([('free', 'Free'),('paid', 'Paid'), ], string='Mode')
    state = fields.Selection([('draft', 'Draft'),
        ('accept', 'Accepted'),
        ('reject', 'Rejected'),],default='draft', string='Status')

    @api.onchange('event_date_start', 'event_date_end', )
    def period_oncahge_validation(self):
        for s in self:
            if s.event_date_end and s.event_date_start:
                if s.event_date_end < s.event_date_start:
                    raise ValidationError("Event End date Should be greater than Start Date!")

    @api.onchange('category_type','event_mode')
    def _onchange_data_get(self):
        type_list = []
        t_ids=None
        if self.event_mode:
            if self.event_mode =='free':
                t_ids=self.env['event.type'].search([('use_ticketing','=',False)])
            if self.event_mode =='paid':
                t_ids=self.env['event.type'].search([('use_ticketing','=',True)])
            for t_id in t_ids:
                type_list.append(t_id.id)
            return {'domain': {'event_type_id': [('id', 'in', type_list)]}}
        else:
            return {'domain': {'event_type_id': [('id', 'in', type_list)]}}

    def action_accept_button(self):
        self.state='accept'
        self.write({'state': 'accept'})
        self.message_post(body=_("%s has accepted invitation") % (self.mentor_id.name))


    def action_reject_button(self):
        self.state='reject'
        self.write({'state': 'reject'})
        self.message_post(body=_("%s has Reject invitation") % (self.mentor_id.name))



    @api.multi
    def get_interval(self, interval, tz=None):
        """ Format and localize some dates to be used in email templates
            :param string interval: Among 'day', 'month', 'dayname' and 'time' indicating the desired formatting
            :param string tz: Timezone indicator (optional)
            :return unicode: Formatted date or time (as unicode string, to prevent jinja2 crash)
        """
        self.ensure_one()
        date = fields.Datetime.from_string(self.event_date_start)
        if tz:
            timezone = pytz.timezone(tz or 'UTC')
            date = date.replace(tzinfo=pytz.timezone('UTC')).astimezone(timezone)
        if interval == 'day':
            # Day number (1-31)
            result = pycompat.text_type(date.day)
        elif interval == 'month':
            # Localized month name and year
            result = babel.dates.format_date(date=date, format='MMMM y', locale=self._context.get('lang') or 'en_US')
        elif interval == 'dayname':
            # Localized day name
            result = babel.dates.format_date(date=date, format='EEEE', locale=self._context.get('lang') or 'en_US')
        elif interval == 'time':
            # Localized time
            # FIXME: formats are specifically encoded to bytes, maybe use babel?
            dummy, format_time = self._get_date_formats()
            result = tools.ustr(date.strftime(format_time + " %Z"))
        return result

    @api.model
    def _get_date_formats(self):
        """ get current date and time format, according to the context lang
            :return: a tuple with (format date, format time)
        """
        lang = self._context.get("lang")
        lang_params = {}
        if lang:
            record_lang = self.env['res.lang'].with_context(active_test=False).search([("code", "=", lang)], limit=1)
            lang_params = {
                'date_format': record_lang.date_format,
                'time_format': record_lang.time_format
            }
        # formats will be used for str{f,p}time() which do not support unicode in Python 2, coerce to str
        format_date = pycompat.to_native(lang_params.get("date_format") or '%B-%d-%Y')
        format_time = pycompat.to_native(lang_params.get("time_format") or '%I-%M %p')
        return (format_date, format_time)

class EventEvent(models.Model):
    _inherit = 'event.event'

    mentor_id = fields.Many2one('res.partner', string='Mentor')
    event_schedule_id=fields.Many2one('event.schedule',"Mentor Schedule")
    mentor_event=fields.Boolean("Mentor Session")
    is_normal = fields.Boolean("Is Normal", default=True)