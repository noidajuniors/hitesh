from odoo import api, fields, models, _
from odoo.exceptions import UserError, Warning, ValidationError


class EquitySales(models.Model):
    _name = 'equity.sale'
    _inherit = ['mail.thread']
    _description = "Equity sale"
    _rec_name = 'name'

    READONLY_STATES = {
        'inprocess': [('readonly', True)],
        'payment_process': [('readonly', True)],
        'paid': [('readonly', True)], }

    state = fields.Selection([
        ('draft', 'Draft'),
        ('initiate', 'Initiate'),
        ('inprocess', 'Inprocess'),
        ('payment_process', 'Payment Inprocess'),
        ('paid', 'Paid'),
        ('cancelled', 'Cancelled'),
    ], string='Status', default='draft')
    name = fields.Char('Reference', copy=False, default=lambda self: _('/'), readonly=True)
    incubatee_id = fields.Many2one('res.partner', string="Incubatee", states=READONLY_STATES, )
    offer_date = fields.Datetime("Offer Date", states=READONLY_STATES, )
    offer_valid_date = fields.Datetime("Offer Validity Date", states=READONLY_STATES, )
    equity_sale_line = fields.One2many('equity.sale.line', 'equity_sale_id', states=READONLY_STATES, )
    total_share = fields.Monetary("Total Share")
    price_per_share = fields.Monetary("Price Per Share")
    equity_percent = fields.Float(string='Equity%')
    applicable_share = fields.Monetary("Applicable Share")
    share_value = fields.Monetary("Share Value", )
    invoice_id = fields.Many2one('account.invoice', string="Invoice Number", copy=False)
    invoice_count = fields.Integer("Invoice", copy=False)
    payment_ids = fields.Many2many('account.payment', string="Payments", copy=False, readonly=True)
    payment_count = fields.Integer("Payment's")
    button_hide = fields.Boolean("Hide")
    document_ids = fields.One2many('upload.document', 'equity_sale_id', 'Audit Report', tates=READONLY_STATES, )
    company_id = fields.Many2one('res.company', string='Company', track_visibility='always', copy=False,
                                 default=lambda self: self.env.user.company_id.id)
    currency_id = fields.Many2one('res.currency', related='company_id.currency_id', readonly=True)
    share_to_sale = fields.Monetary("Share to Be Sold", states=READONLY_STATES, default=1.0)

    @api.onchange('incubatee_id')
    def update_incubatee_details(self):
        if self.incubatee_id:
            self.equity_percent = self.incubatee_id.equity_percent
            self.total_share = self.incubatee_id.total_share
            self.price_per_share = self.incubatee_id.price_per_share
            self.equity_percent = self.incubatee_id.equity_percent
            self.share_value = self.incubatee_id.share_value
            self.applicable_share = self.incubatee_id.applicable_share

    # ==========================**Action for Invoice Vieww===========
    @api.multi
    def action_view_invoice(self):
        invoice = self.sudo().mapped('invoice_id')
        action = self.env.ref('account.action_invoice_tree1').read()[0]
        if len(invoice) == 1:
            action['views'] = [(self.env.ref('account.invoice_form').id, 'form')]
            action['res_id'] = invoice.id
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    # ===========================**Action for PAYMNET===============
    @api.multi
    def action_view_payment(self):
        payments = self.mapped('payment_ids')
        action = self.env.ref('account.action_account_payments').read()[0]
        if len(payments) > 1:
            action['domain'] = [('id', 'in', payments.ids)]
        elif len(payments) == 1:
            action['views'] = [(self.env.ref('gts_coe_management.custom_account_payment_form_view').id, 'form')]
            action['res_id'] = payments.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    @api.multi
    def action_confirm(self):
        if self.share_to_sale > self.applicable_share:
            raise UserError(_("Sorry! You can not sale Equity more than Available!"))
        self.name = self.env['ir.sequence'].next_by_code('equity.sale.sequence') or _('')
        self.state = 'initiate'
        for line in self.equity_sale_line:
            line.state = 'initiate'

    @api.multi
    def action_invoice_generate(self):
        account_id = None
        product_id = self.env['product.product'].search([('equity_type', '=', True), ('type', '=', 'service')]) or None
        if not product_id:
            raise UserError(_('Please Create Product for Equity Service Type To Raise Invoice Payment '))
        if not self.equity_sale_line:
            raise UserError(_('Please Add Equity Details Line'))
        account_id = self.env['account.account'].search([('user_type_id', '=', 'Income')], limit=1)
        self.state = 'payment_process'
        if self.equity_sale_line:
            data_id = self.equity_sale_line.filtered(lambda eqt: eqt.state in ('inprocess')) or None
            if data_id:
                data_id.state = 'payment_process'
                inv_id = self.env['account.invoice'].create({
                    'origin': self.name,
                    'type': 'out_invoice',
                    'state': 'draft',
                    'equity_sale_id': self.id,
                    'equity_boolean': True,
                    'partner_id': data_id.partner_id.id,
                })
                line_id = self.env['account.invoice.line'].create({
                    'name': "EquitySale/" + self.name,
                    'origin': "EquitySale/" + self.name,
                    'account_id': account_id.id if account_id else data_id.partner_id.property_account_payable_id.id,
                    'price_unit': data_id.approved_value if data_id.approved_value else self.price_per_share,
                    'quantity': self.applicable_share,
                    'product_id': product_id.id,
                    'product_uom_id': product_id.product_uom_id.id,
                    'invoice_id': inv_id.id, })
                self.invoice_id = inv_id
                self.invoice_count = len(self.invoice_id)
                action = self.env.ref('account.action_invoice_tree1').read()[0]
                if len(self.invoice_id) == 1:
                    action['views'] = [(self.env.ref('account.invoice_form').id, 'form')]
                    action['res_id'] = self.invoice_id.id
                    return action


class EquitySalesLine(models.Model):
    _name = 'equity.sale.line'
    _inherit = ['mail.thread']
    _description = "Equity sale line"

    @api.depends('partner_id', 'approved_value', 'current_value')
    def compute_approved_value(self):
        for s in self:
            if s.approved_value <= 0:
                s.total_value = s.equity_sale_id.price_per_share * s.equity_sale_id.share_to_sale
            if s.approved_value > 0:
                s.total_value = s.approved_value * s.equity_sale_id.share_to_sale

    company_id = fields.Many2one('res.company', string='Company', track_visibility='always', copy=False,
                                 default=lambda self: self.env.user.company_id.id)
    currency_id = fields.Many2one('res.currency', related='company_id.currency_id', readonly=True)
    equity_sale_id = fields.Many2one('equity.sale')
    partner_id = fields.Many2one('res.partner', "Customer name")
    description = fields.Char("Description")
    current_value = fields.Monetary("Current Value", related='equity_sale_id.price_per_share', store=True)
    offer_value = fields.Monetary("Offer Value")
    approved_value = fields.Monetary("Approved Value")
    total_value = fields.Monetary("Total Value", compute='compute_approved_value', store=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('initiate', 'Initiate'),
        ('inprocess', 'Inprocess'),
        ('payment_process', 'Payment Inprocess'),
        ('paid', 'Paid'),
        ('cancelled', 'Cancelled'),
    ], string='Status', default='draft')

    @api.multi
    def equity_approve(self):
        line_ids = self.equity_sale_id.equity_sale_line.filtered(lambda eqt: eqt.state not in ('inprocess')) or None
        for line in line_ids:
            line.state = 'cancelled'
        self.state = 'inprocess'
        self.equity_sale_id.state = 'inprocess'

    @api.multi
    def equity_cancel(self):
        self.state = 'cancelled'


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    equity_sale_id = fields.Many2one('equity.sale', string="Equity Sale")

    @api.multi
    def action_invoice_paid(self):
        res = super(AccountInvoice, self).action_invoice_paid()
        if self.equity_sale_id:
            self.equity_sale_id.write(
                {'payment_ids': [(6, 0, self.payment_ids.ids)], 'payment_count': len(self.payment_ids)})
            if self.state == 'paid':
                self.equity_sale_id.write({'state': 'paid', })
                self.equity_sale_id.incubatee_id.write({
                                                           'applicable_share': self.equity_sale_id.incubatee_id.applicable_share - self.equity_sale_id.share_to_sale, })
                data_id = self.equity_sale_id.equity_sale_line.filtered(lambda eqt: eqt.state in ('payment_process'))
                data_id.state = 'paid'

        return res
