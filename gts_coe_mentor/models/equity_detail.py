from odoo import api, fields, models, _
from odoo.exceptions import UserError, Warning, ValidationError
from datetime import datetime, time


class ProductProduct(models.Model):
    _inherit = "product.product"
    _description = "Equity Management"

    equity_type = fields.Boolean("Equity Type")


class EquityDetail(models.Model):
    _name = 'equity.detail'
    _inherit = ['mail.thread']
    _description="Equity Management"
    _rec_name='name'

    READONLY_STATES = {
        'confirm': [('readonly', True)],
        'payment': [('readonly', True)],
        'reject': [('readonly', True)],
        'paid': [('readonly', True)], }

    @api.multi
    @api.depends('total_share','equity_percent','price_per_share')
    def _find_share_value(self):
        for s in self:
            s.applicable_share=(s.total_share*s.equity_rage_id.percentage/100)
            s.share_value=s.applicable_share*s.price_per_share

    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirm'),
        ('payment', 'Payment Requested'),
        ('paid', 'Paid')
        ], string='Status', default='draft')
    equity_type = fields.Selection([('fresh', 'Fresh'),('transferred', 'Transferred'),], states=READONLY_STATES,string='Equity Type', default='fresh')
    mode_of_purchase = fields.Selection([('dd', 'DD'),('Cheque', 'Cheque'), ],states=READONLY_STATES, string='Mode of Purchase', default='cash')
    name = fields.Char('Reference', copy=False, default=lambda self: _('/'), readonly=True)
    total_share=fields.Monetary("Total Share",states=READONLY_STATES,)
    price_per_share=fields.Monetary("Price Per Share",states=READONLY_STATES,)
    attachment_id=fields.Binary("Share Certificate Upload",states=READONLY_STATES,)
    attachment_name=fields.Char("Attach Name")
    share_value = fields.Monetary("Share Value",compute='_find_share_value',states=READONLY_STATES,)
    applicable_share = fields.Monetary("Applicable Share",compute='_find_share_value')
    invoice_id = fields.Many2one('account.invoice', string="Invoice Number", copy=False)
    invoice_count=fields.Integer("Invoice",copy=False)
    incubatee_id = fields.Many2one('res.partner', string="Incubatee",states=READONLY_STATES,)
    equity_percent = fields.Float(string='Equity%',states=READONLY_STATES,)
    res_equity_id=fields.Many2one('res.partner')
    payment_ids = fields.Many2many('account.payment',string="Payments", copy=False, readonly=True)
    payment_count=fields.Integer("Payment's")
    date=fields.Datetime("Created Date",default=fields.Datetime.now())
    created_by=fields.Many2one('res.users',"Created By" ,default=lambda self: self.env.user)
    confirmed_by=fields.Many2one('res.users',"Confirmed By", default=lambda self: self.env.user)
    company_id = fields.Many2one('res.company', string='Company', track_visibility='always', copy=False,default=lambda self: self.env.user.company_id.id)
    currency_id = fields.Many2one('res.currency', related='company_id.currency_id', readonly=True)
    equity_rage_id = fields.Many2one('coe.equity.range', 'Equity Slab',states=READONLY_STATES,)
    coe_id = fields.Many2one('coe.setup.master','COE')
    color = fields.Integer("Color Index", default=0)



    @api.onchange('incubatee_id')
    def update_percent(self):
        self.coe_id = self.incubatee_id.coe_id.id

    @api.onchange('equity_rage_id')
    def update_percentage(self):
        if self.equity_rage_id:
            self.equity_percent = self.equity_rage_id.percentage

    # *********** Action to return on invoice*************
    @api.multi
    def action_view_invoice(self):
        invoice = self.sudo().mapped('invoice_id')
        action = self.env.ref('account.action_vendor_bill_template').read()[0]
        if len(invoice) == 1:
            action['views'] = [(self.env.ref('account.invoice_supplier_form').id, 'form')]
            action['res_id'] = invoice.id
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

# ***************Action for PAYMNET*******************
    @api.multi
    def action_view_payment(self):
        payments = self.mapped('payment_ids')
        action = self.env.ref('account.action_account_payments_payable').read()[0]
        if len(payments) > 1:
            action['domain'] = [('id', 'in', payments.ids)]
        elif len(payments) == 1:
            action['views'] = [(self.env.ref('gts_coe_management.custom_account_payment_form_view').id, 'form')]
            action['res_id'] = payments.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    # ******update data of equity in incubatee*****************
    @api.multi
    def action_confirm(self):
        applicable_share=self.incubatee_id.applicable_share or 0
        total_share=self.incubatee_id.total_share or 0
        self.name = self.env['ir.sequence'].next_by_code('equity.sequence') or _('')
        self.equity_percent = self.equity_rage_id.percentage
        self.state='confirm'

        self.incubatee_id.write({
            'equity_percent':self.equity_percent,
            'total_share':self.total_share+total_share,
            'price_per_share':self.price_per_share,
            'share_value':self.share_value,
            'applicable_share':self.applicable_share +applicable_share,
            'created_by':self.created_by.id,
            'confirmed_by':self.confirmed_by.id,
            'date':self.date,
            'equity_detail_bool':True
            })


    @api.multi
    def action_initiate_payment(self):
        account_id=None
        self.state = 'payment'
        product_id=self.env['product.product'].search([('equity_type','=',True), ('type','=','service')]) or None
        if not product_id:
            raise UserError(_('Please Create Product for Equity Service Type To Raise Invoice Payment '))
        account_id = self.env['account.account'].search([('user_type_id', '=', 'Income')], limit=1)
        inv_id=self.env['account.invoice'].create({
                            'origin': self.name,
                            'type': 'in_invoice',
                             'state':'draft',
                             'equity_id':self.id,
                             'partner_id':self.incubatee_id.id,
                             })
        line_id=self.env['account.invoice.line'].create({
            'name': "Purchase Of Equity/"+ self.name,
            'origin': "Purchase Of Equity/"+ self.name,
            'account_id': account_id.id if account_id else self.incubatee_id.property_account_payable_id.id,
            'price_unit': self.price_per_share,
            'quantity': self.applicable_share,
            'product_id':product_id.id,
            'product_uom_id':product_id.product_uom_id.id,
            'invoice_id': inv_id.id,})
        self.invoice_id=inv_id
        self.invoice_count=len(self.invoice_id)
        self.incubatee_id.write({'profile_status': 'equity'})
        action = self.env.ref('account.action_vendor_bill_template').read()[0]
        if len(self.invoice_id) == 1:
            action['views'] = [(self.env.ref('account.invoice_supplier_form').id, 'form')]
            action['res_id'] = self.invoice_id.id
            return action


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    equity_id = fields.Many2one('equity.detail', string="Equity Purchase")

    @api.multi
    def action_invoice_paid(self):
        res = super(AccountInvoice, self).action_invoice_paid()
        if self.equity_id:
            self.equity_id.write({'payment_ids': [(6, 0, self.payment_ids.ids)],'payment_count': len(self.payment_ids)})
            if self.state == 'paid':self.equity_id.write({'state': 'paid',})
        return res
