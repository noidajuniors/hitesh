from odoo import api, fields, models, _
from odoo.exceptions import UserError, Warning, ValidationError
from datetime import datetime, time


class MentorshipServiceDiscontinue(models.Model):
    _name = 'mentorship.service.discontinue'
    _custom = False
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'name'
    _description = 'Discontinue Services'

    READONLY_STATES = {
        'approved': [('readonly', True)],
        'request': [('readonly', True)],
        'completed': [('readonly', True)],
    }
    name = fields.Char('Reference', copy=False, default=lambda self: _('/'), readonly=True)
    discontinue_line_id = fields.One2many('mentorship.service.discontinue.line', 'discontinue_id',
                                          string="Discontinue Line", states=READONLY_STATES, )
    requested_date = fields.Datetime("Requested Date", default=fields.Datetime.now(), readonly="1")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('request', 'Request'),
        ('approved', 'Approved'),
        ('cancel', 'Cancel'),
        ('reject', 'Reject'),
        ('completed', 'Completed'),
    ], string='Status', copy=False, default='draft', track_visibility='onchange')
    remarks = fields.Char("Reason", states=READONLY_STATES, )
    approve_date = fields.Datetime("Approved Date", readonly="1")
    approve_by = fields.Many2one('res.users', 'Approved By', readonly="1")
    partner_id = fields.Many2one('res.partner', 'Requested By', states=READONLY_STATES, )
    mentor_id = fields.Many2one('res.partner', 'Mentor', states=READONLY_STATES, )

    request_type = fields.Selection([('mentor', 'Mentor'), ('incubatee', 'Incubatee'), ], states=READONLY_STATES,
                                    string='Request By', copy=False, default='mentor', track_visibility='onchange')
    incubatee_id = fields.Many2one('res.partner', domain=[('coe_partner_type', '=', 'incubatee')],
                                   states=READONLY_STATES,default=lambda self: self.env.user.partner_id)

    @api.multi
    def request_button(self):
        self.state = 'request'
        self.name = self.env['ir.sequence'].next_by_code('mentorship.service.discontinue') or _('')

    @api.multi
    def approve_button(self):
        self.state = 'approved'
        self.approve_date = fields.Datetime.now()
        self.approve_by = self.env.uid

    @api.multi
    def accept_button(self):
        self.state = 'completed'
        for line in self.discontinue_line_id:
            # if line.category_name_id.strategic_bool==True:
            for inc_line in line.incubatee_id.strategic_mentorship_line:
                if inc_line.category_name_id == line.category_name_id:
                    inc_line.end_date = datetime.today()
                    line.incubatee_id.strategic_mentor = None

    @api.multi
    def reject_button(self):
        self.state = 'reject'

    @api.multi
    def cancel_button(self):
        self.state = 'cancel'


class MentorshipServiceDiscontinueLine(models.Model):
    _name = 'mentorship.service.discontinue.line'
    _custom = False

    discontinue_id = fields.Many2one('mentorship.service.discontinue')
    mentorship_request_id = fields.Many2one('mentorship.request', string="Request")
    mentorship_request_incubatee = fields.Many2one('mentorship.request', string="Request")
    incubatee_id = fields.Many2one('res.partner', 'Requested By')
    category_name_id = fields.Many2one('mentor.category', string='Mentor Category')
    partner_id = fields.Many2one('res.partner', 'Mentor')
    commencement_date = fields.Datetime("Start Date")
    duration_type = fields.Selection([('hrs', 'Hrs'), ('day', 'Days'), ('month', 'Months')], string="Duration Type")
    duration = fields.Float("Duration")
    remarks = fields.Char('Remarks')
    requested_date = fields.Datetime("Requested Date")

    @api.onchange('mentorship_request_id', 'mentorship_request_incubatee')
    def onchange_mentor_req(self):
        if self.mentorship_request_id:
            # self.update({
            #     'incubatee_id': self.mentorship_request_id.incubatee_id.id,
            #     'requested_date': self.mentorship_request_id.requested_date,
            #     'remarks': self.mentorship_request_id.remarks,
            #     'category_name_id': self.mentorship_request_id.category_name_id.id,
            #     'commencement_date': self.mentorship_request_id.date,
            # })

            self.incubatee_id = self.mentorship_request_id.incubatee_id.id
            self.requested_date = self.mentorship_request_id.requested_date
            self.remarks = self.mentorship_request_id.remarks
            self.category_name_id = self.mentorship_request_id.category_name_id.id
            self.commencement_date = self.mentorship_request_id.date
        if self.mentorship_request_incubatee:
            self.incubatee_id = self.mentorship_request_incubatee.incubatee_id.id
            self.requested_date = self.mentorship_request_incubatee.requested_date
            self.remarks = self.mentorship_request_incubatee.remarks
            self.category_name_id = self.mentorship_request_incubatee.category_name_id.id
            self.commencement_date = self.mentorship_request_incubatee.date
