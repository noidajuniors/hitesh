from odoo import api, fields, models, _
from datetime import timedelta, date, datetime
import calendar
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, Warning, ValidationError


class AgreementType(models.Model):
    _name = 'agreement.type'
    _custom = False
    _rec_name = 'name'
    name = fields.Char('Name')
    description = fields.Char('Description')


class HonoryType(models.Model):
    _name = 'honory.type'
    _custom = False

    name = fields.Char('Name')
    description = fields.Char('Description')


class MentorshipRequest(models.Model):
    _name = 'mentorship.request'
    _custom = False
    _rec_name = 'name'
    _description = "Mentorship Request"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    READONLY_STATES = {
        'approved': [('readonly', True)],
        'cancel': [('readonly', True)],
        'reject': [('readonly', True)],
        'completed': [('readonly', True)], }

    READONLY_STATES1 = {
        'completed': [('readonly', True)],
        'cancel': [('readonly', True)],
        'reject': [('readonly', True)], }

    @api.depends('price', 'number_of_equity')
    def _find_total(self):
        for s in self:
            s.total = s.number_of_equity * s.price

    @api.onchange('incubatee_id')
    def def_onchange_cat(self):
        mentor_cat=[]
        coe_id = self.env['coe.setup.master'].search([('partner_id', '=', self.incubatee_id.id)])
        mentor_ids = self.env['coe.mentor.line'].search([('coe_id', '=',coe_id.id)])
        for m_id in mentor_ids:
            mentor_cat.append(m_id.category_name_id.id)
        return {'domain': {'category_name_id': [('id', 'in', mentor_cat)]}}

    name = fields.Char('Reference', copy=False, default=lambda self: _('/'), readonly=True)
    partner_id = fields.Many2one('res.partner', 'Mentor', states=READONLY_STATES, )
    discontinue_id = fields.Many2one('mentorship.service.discontinue', 'Discontinue Process', states=READONLY_STATES, )
    incubatee_id = fields.Many2one('res.partner', 'Requested By', states=READONLY_STATES, default=lambda self: self.env.user.partner_id)
    category_name_id = fields.Many2one('mentor.category', string='Mentor Category', states=READONLY_STATES, )
    service_loc = fields.Many2one('coe.service.location', string='Service Location', states=READONLY_STATES, )
    requested_by = fields.Many2one('res.users', string='Requested by', default=lambda self: self.env.user,
                                   states=READONLY_STATES, )
    nationality = fields.Many2one('res.country', string='Nationality', states=READONLY_STATES, )
    added_by = fields.Many2one('res.users', string='Added By', states=READONLY_STATES, )
    remarks = fields.Char('Remarks', states=READONLY_STATES1, )
    state = fields.Selection([
        ('draft', 'Draft'),
        ('request', 'Requested'),
        ('approved', 'Approved'),
        ('cancel', 'Cancelled'),
        ('reject', 'Rejected'),
        ('completed', 'Completed'),
    ], string='Status', copy=False, default='draft', track_visibility='onchange')
    commercial_detail = fields.Text("Commercial Agreement ", states=READONLY_STATES1, )
    aggrement_detail = fields.Text("Details of Agreement", states=READONLY_STATES1, )
    company_id = fields.Many2one('res.company', string='Company', change_default=True, default=lambda self: self.env.user.company_id.id)

    # company_id = fields.Many2one('res.partner', string='Company', states=READONLY_STATES, )
    company_name = fields.Char(string='Company Name', states=READONLY_STATES, )
    type_of_company = fields.Selection([
        ('Pvt_ltd', 'Pvt ltd'),
        ('Partnership', 'Partnership'),
        ('proprietor', 'Proprietor'),
        ('yet_to_be_formed', 'Yet to be Formed')])
    coe_id = fields.Many2one('coe.setup.master', 'COE', states=READONLY_STATES, )
    team_size = fields.Integer("Team Size")
    narration = fields.Integer("Narration")
    commercial_type = fields.Selection([('equity', 'Equity'), ('consideration', 'Consideration'), ('honory', 'Honory')],
                                       states=READONLY_STATES1, string="Commercial Type")
    type_of_agreement = fields.Many2one('agreement.type', "Type of agreement", states=READONLY_STATES1, )
    description = fields.Char('Description', states=READONLY_STATES1, )
    number_of_equity = fields.Integer("No.Of Equity", states=READONLY_STATES1, )
    price = fields.Float("Price/Share", states=READONLY_STATES1, )
    total = fields.Float("Total", store=True, compute='_find_total')
    document_ids = fields.One2many('upload.document', 'partner_id', 'Upload Documents', states=READONLY_STATES1, )
    honory_type = fields.Many2one('honory.type', string="Honory Type", states=READONLY_STATES1, )
    duration_type = fields.Selection([('hrs', 'Hrs'), ('day', 'Days'), ('month', 'Months')], string="Duration Type",
                                     states=READONLY_STATES, )
    duration = fields.Float("Duration", states=READONLY_STATES, )
    requested_date = fields.Datetime("Requested Date", default=fields.Datetime.now(), readonly=True)
    date = fields.Datetime("Start Date", states=READONLY_STATES)
    commencement_date = fields.Datetime("End Date", states=READONLY_STATES, )
    approved_date = fields.Datetime("Approved Date", readonly=True)
    strategic_bool = fields.Boolean(related='category_name_id.strategic_bool', String="Strategic", store=True)
    mentorship_type = fields.Selection([('online', 'Online'), ('offline', 'Offline')], default='offline',
                                       string="Mentorship Type", states=READONLY_STATES)

    @api.multi
    def write(self, vals):
        if vals.get('incubatee_id') or vals.get('category_name_id'):
            r_id = self.env['mentorship.request'].search(
                [('incubatee_id', '=', vals.get('incubatee_id')), ('category_name_id.strategic_bool', '=', True),
                 ('state', 'in', ('approved', 'completed'))])
            if r_id:
                raise UserError(
                    _("You Can not request Strategic Mentor Again, Since its already mapped into your profile."))
        return super(MentorshipRequest, self).write(vals)

    @api.model
    def create(self, vals):
        res = super(MentorshipRequest, self).create(vals)
        if res.category_name_id.strategic_bool == True:
            r_id = self.env['mentorship.request'].search(
                [('incubatee_id', '=', vals.get('incubatee_id')), ('category_name_id.strategic_bool', '=', True),
                 ('state', 'in', ('approved', 'completed'))])
            if r_id:
                raise UserError(
                    _(" You Can not request Strategic Mentor Again, Since its already mapped into your profile."))
        m_id = self.env['mentorship.request'].sudo().search(
            [('date', '<=', vals.get('date')), ('commencement_date', '<', vals.get('commencement_date'))])
        if m_id:
            raise ValidationError(_("You can't request Mentorship with %s %s date duration \
            Since Selected Slot is not available for this mentor!") %
                                  ((vals.get('date'), vals.get('commencement_date'))))
        return res

    @api.multi
    def information_button(self):
        return {
            'name': _('Mentor Details'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'res.partner',
            'view_id': self.env.ref('gts_coe_mentor.new_res_partner_form_view_mentor').id,
            'type': 'ir.actions.act_window',
            'res_id': self.partner_id.id,
            'target': 'new',
            'context': ({'default_view_readonly': True})
        }

    @api.constrains('duration')
    @api.onchange('duration', 'date', 'duration_type')
    def find_commencement_date(self):
        if self.date and self.duration_type and self.duration:
            if self.duration_type == 'day':
                self.commencement_date = self.date + timedelta(days=self.duration)
            if self.duration_type == 'hrs':
                self.commencement_date = self.date + timedelta(hours=self.duration)
            if self.duration_type == 'month':
                self.commencement_date = self.date + relativedelta(months=int(self.duration))

    @api.onchange('partner_id', 'incubatee_id')
    def find_tota(self):
        self.total = self.price * self.number_of_equity

    @api.constrains('incubatee_id')
    @api.onchange('partner_id', 'incubatee_id')
    def mentor_data(self):
        for s in self:
            s.nationality = s.incubatee_id.country_id.id
            s.coe_id = s.incubatee_id.coe.id
            s.team_size = s.incubatee_id.team_size
            s.company_id = s.incubatee_id.company_id.id
            s.company_name = s.incubatee_id.company_name
            s.type_of_company = s.incubatee_id.type_of_company

    @api.multi
    def reject_button(self):
        # ============Activity delete==============
        activities = self.env['mail.activity'].search([('id', 'in', self.activity_ids.ids)])
        for activity in activities:
            activity.unlink()
        self.state = 'reject'

    @api.multi
    def cancel_button(self):
        self.state = 'cancel'

    @api.multi
    def request_button(self):
        user_list=[]
        request_id = self.env['mentorship.request'].search(
            [('incubatee_id', '=', self.incubatee_id.id),
             ('category_name_id', '=', self.category_name_id.id),
             ('category_name_id.strategic_bool', '=', True),
             ('state', 'in', ('approved', 'completed'))])
        if request_id:
            raise UserError(
                _(" You Can not Request Strategic Mentor Again, Since its already mapped into your profile."))
        self.state = 'request'
        self.name = self.env['ir.sequence'].next_by_code('mentorship.request.seq') or _('')

        # ============ To Create Service Activity  ==============
        activity_type = self.env['mail.activity.type'].search([('is_notify_type', '=', True)])
        if self.incubatee_id and self.incubatee_id.parent_id:
            res_user_ids = self.env['res.users'].search([('entity_id', '=', self.incubatee_id.parent_id.id)])
            if not res_user_ids:
                res_user_ids = self.env['res.users'].search([('partner_id', '=', self.incubatee_id.parent_id.id)])
            if res_user_ids:
                for user in res_user_ids:
                    user_list.append(user)
        mentor_user_id = self.env['res.users'].search([('partner_id', '=', self.partner_id.id)])
        if mentor_user_id:
            user_list.append(mentor_user_id)
        if not activity_type:
            raise UserError(_("Please Configure Activity Type."))
        for user_id in user_list:
            activity_id = self.env['mail.activity'].create({
                'res_model': 'mentorship.request',
                'res_model_id': self.env.ref('gts_coe_mentor.model_mentorship_request').id,
                'res_id': self.id,
                'activity_type_id': activity_type.id,
                'user_id': user_id.id,
                'summary': "Mentorship Request-" + str(self.name) + "Requested By-" + str(self.incubatee_id.name)
            })


        template_id = self.env.ref('gts_coe_mentor.email_template_for_mentorship_request1')
        view_id = self.env.ref('gts_coe_mentor.action_mentorship_request_master')
        if template_id:
            custom_url = ('web#id=' + str(self.id) + '&action=' + str(
                view_id.id) + '&model=mentorship.request&view_type=form')
            values = template_id.generate_email(self.id)
            values['email_to'] = self.partner_id.email
            values['email_from'] = self.env.user.partner_id.email
            values['email_cc'] = self.incubatee_id.parent_id.email
            values['body_html'] = values['body_html'].replace('_customer_url', custom_url)
            mail = self.env['mail.mail'].create(values)
            try:
                mail.send()
            except Exception:
                pass

    @api.multi
    def approve_button(self):
        self.state = 'approved'
        e_list = []
        self.approved_date = datetime.now()
        if self.partner_id:
            if self.partner_id.incubatee_ids:
                for line in self.partner_id.incubatee_ids:
                    e_list.append(line.id)
                e_list.append(self.incubatee_id.id)
            else:
                e_list.append(self.incubatee_id.id)
            self.partner_id.incubatee_ids = [(6, 0, e_list)]

        # ============Activity delete==============
        activities = self.env['mail.activity'].search([('id', 'in', self.activity_ids.ids)])
        for activity in activities:
            activity.unlink()

        template_id = self.env.ref('gts_coe_mentor.email_template_for_mentorship_request1')
        view_id = self.env.ref('gts_coe_mentor.action_mentorship_request_master')
        if template_id:
            custom_url = ('web#id=' + str(self.id) + '&action=' + str(
                view_id.id) + '&model=mentorship.request&view_type=form')
            values = template_id.generate_email(self.id)
            values['email_to'] = self.partner_id.email
            values['email_from'] = self.env.user.partner_id.email
            values['email_cc'] = self.incubatee_id.parent_id.email
            values['body_html'] = values['body_html'].replace('_customer_url', custom_url)
            mail = self.env['mail.mail'].create(values)
            try:
                mail.send()
            except Exception:
                pass

    def accept_button(self):
        if not self.document_ids:
            UserError(_("Please Attach Agreement Document."))
        self.state = 'completed'

        if self.strategic_bool == True:
            self.incubatee_id.strategic_mentor = self.partner_id.id
        strategic_line = {
            'strategic_id': self.incubatee_id.id,
            'mentor_id': self.partner_id.id,
            'category_name_id': self.category_name_id.id,
            'start_date': datetime.today(),
            'mentor_request_id': self.id
        }

        self.env['strategic.mentorship.history'].create(strategic_line)
        template_id = self.env.ref('gts_coe_mentor.email_template_for_mentorship_request1')
        view_id = self.env.ref('gts_coe_mentor.action_mentorship_request_master')
        if template_id:
            custom_url = ('web#id=' + str(self.id) + '&action=' + str(
                view_id.id) + '&model=mentorship.request&view_type=form')
            values = template_id.generate_email(self.id)
            values['email_to'] = self.incubatee_id.email
            values['email_from'] = self.partner_id.email
            values['email_cc'] = self.incubatee_id.parent_id.email
            values['body_html'] = values['body_html'].replace('_customer_url', custom_url)
            mail = self.env['mail.mail'].create(values)
            try:
                mail.send()
            except Exception:
                pass


class UploadDocument(models.Model):
    _inherit = 'upload.document'

    mentorship_id = fields.Many2one('mentorship.request', "Mentorship")
    forum_id = fields.Many2one('mentorship.forum', "Mentorship Fourm")
    equity_evl_id = fields.Many2one('equity.evaluation', "Equity Evaluation")
    equity_sale_id = fields.Many2one('equity.sale', "Equity Sales")
