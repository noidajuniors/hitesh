from odoo import api, fields, models


class IncubateeMentorMapping(models.Model):
    _name = "incubatee.mentor.mapping"
    _rec_name = "mentor_id"

    mentor_id = fields.Many2one('res.partner', domain=[('coe_partner_type', '=', 'mentor')])
    incubatee_id = fields.Many2one('res.partner', domain=[('coe_partner_type', '=', 'incubatee')])
    agreement_detail = fields.Char("Details of Agreement ")
    comercial_detail = fields.Char("Commercial Agreement")
    states = fields.Selection([
        ('draft', 'Draft'),
        ('wip', 'Waiting For Approval'),
        ('Approved', 'Approved'),
        ('cancel', 'Cancelled')], string='State',
        copy=False, default='draft', track_visibility='onchange')

    @api.multi
    def confirm_button(self):
        self.states = 'wip'

    @api.multi
    def cancel_button(self):
        self.states = 'cancel'

    @api.multi
    def approve_button(self):
        self.states = 'Approved'
