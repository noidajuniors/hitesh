from odoo import api, fields, models
import datetime
from ast import literal_eval


class CoeAlumini(models.Model):
    _name = "coe.alumini"
    _rec_name = 'incubatee_id'
    _inherit = ['mail.thread']
    _description = "COE Alumini"

    employee_strength = fields.Integer("Current Employee Strength")
    current_revenue = fields.Float("Current Revenue ")
    active = fields.Boolean('Active', default=True, track_visibility=True)

    type_of_company = fields.Selection([
        ('Pvt_ltd', 'Pvt ltd'),
        ('Partnership', 'Partnership'),
        ('proprietor', 'Proprietor'),
        ('yet_to_be_formed', 'Yet to be Formed')])
    name = fields.Char(index=True)
    date_of_onboarding = fields.Date("Onboarding Date", )
    current_turnover = fields.Float("Current Turnover")

    incubatee_id = fields.Many2one('res.partner', "Incubatee")
    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char(change_default=True)
    city = fields.Char()
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict',
                               domain="[('country_id', '=?', country_id)]")
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')
    email = fields.Char()
    email2 = fields.Char('Current Emaill')
    phone = fields.Char("Current Phone No")
    mobile = fields.Char()
    city_id = fields.Many2one('res.city', 'City')
    is_company = fields.Boolean(string='Is a Company', default=False,
                                help="Check if the contact is a company, otherwise it is a person")
    category_id = fields.Many2many('res.partner.category')
    website = fields.Char()
    comment = fields.Text(string='Notes')
    color = fields.Integer(string='Color Index', default=0)
    contact_address = fields.Char(string='Complete Address')
    gstin_no = fields.Char("GST")
    pan_no = fields.Char("PAN")
    tan_no = fields.Char("TAN")
    image = fields.Binary("Image", attachment=True,
                          help="This field holds the image used as avatar for this contact, limited to 1024x1024px", )
    image_medium = fields.Binary("Medium-sized image", attachment=True,
                                 help="Medium-sized image of this contact. It is automatically " \
                                      "resized as a 128x128px image, with aspect ratio preserved. " \
                                      "Use this field in form views or some kanban views.")
    coe_partner_type = fields.Selection([('mentor', 'Mentor'),
                                         ('investor', 'Investor'),
                                         ('incubatee', 'Incubatee'),
                                         ('consultant', 'Consultant'),
                                         ('alumini', 'Alumini'),
                                         ('partner', 'Partner'),
                                         ('coe', 'COE'),
                                         ('center', 'Center'),
                                         ('directorate', 'Directorate'),
                                         ('hq', 'HQ'),
                                         ('member', 'Member')], string="COE Type")
    is_incubatee = fields.Boolean('Is a Incubatee')
    coe_id = fields.Many2one('coe.setup.master', string='COE')
    brief = fields.Char('Brief about Team')
    category_name_id = fields.Many2one('mentor.category', string='Mentor Category')
    describe_company = fields.Text('Describe about Company Product/Solution in brief')
    unique_inno = fields.Text('What is unique/innovative about your idea')
    company_id = fields.Many2one('res.partner', string='Company')
    religion_id = fields.Many2one('religion.type', string='Religion')
    availability_mentorship = fields.Text('Availability for Mentorship')
    job_position = fields.Char('Job Position')
    cin = fields.Char('CIN')
    equity = fields.Char('Equity Details (If Any)')
    team_size = fields.Integer('Team Size')
    experience_year = fields.Integer('Years of relevant experience')
    property_payment_term_id = fields.Many2one('account.payment.term', string='Customer Payment Terms', )
    service_ids = fields.Many2many('product.product', 'service_offer', 'service_id', 'res_id', string='Services')
    states = fields.Selection([
        ('draft', 'Draft'),
        ('wip', 'Waiting For Approval'),
        ('Approved', 'Approved'),
        ('cancel', 'Cancelled'),
        ('alumini', 'Alumini')
    ], string='State',
        copy=False, default='draft', track_visibility='onchange')

    director_line_ids = fields.One2many('director.details', 'director_id', string='Director Details')

    @api.multi
    def action_incubatee_showcase(self):
        shoecase_ids = None
        shoecase_ids = self.env['incubatee.showcase'].search([('incubatee_id', '=', self.incubatee_id.id)])
        action = self.env.ref('gts_coe_incubatee.action_incubatee_showcase').read()[0]
        if len(shoecase_ids) > 1:
            action['domain'] = [('id', 'in', shoecase_ids.ids)]
        elif len(shoecase_ids) == 1:
            action['views'] = [(self.env.ref('gts_coe_incubatee.view_showcase_form').id, 'form')]
            action['res_id'] = shoecase_ids.ids[0]
        else:
            action['domain'] = [('id', 'in', shoecase_ids.ids)]
        action['context'] = {'create': False, 'edit': True}
        return action

    @api.model
    def create(self, vals):
        res = super(CoeAlumini, self).create(vals)

        # access_group = self.env.ref('gts_coe_management.group_admin_coe')
        # access_group.write({'users': [(4, user_id.id)]})



