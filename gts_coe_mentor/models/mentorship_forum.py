from odoo import api, fields, models
from odoo.addons.http_routing.models.ir_http import slug


class MentorshipForum(models.Model):
    _name = "mentorship.forum"
    _custom = False
    _inherit = ['mail.thread', 'mail.activity.mixin', 'website.seo.metadata', 'website.published.mixin']
    _rec_name = 'title'

    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting_for_approval', 'Waiting for Approval'),
        ('approved', 'Approved'),
        ('cancel', 'Cancelled'),
        ('reject', 'Rejected')
    ], string='Status', copy=False, default='draft', track_visibility='onchange')
    title = fields.Char("Title")
    html_editor = fields.Html('HTML Editor')
    document = fields.Many2one('document.type', 'Document Type')
    post_type = fields.Selection([('online', 'Online'), ('offline', 'Offline'), ], default='online', string="Post Type")
    created_by = fields.Many2one('res.partner', 'Created by')
    created_date = fields.Datetime("Created Date", default=fields.Datetime.now(), required=True)
    visibility_option = fields.Selection([('everyone', 'Everyone'),
                                          ('user', 'Selected Users'),
                                          ('private', 'Private')], string='Visibility Option')
    text = fields.Text("Summary", copy=False)
    document_ids = fields.One2many('upload.document', 'forum_id', 'Upload Documents')
    partners_ids = fields.Many2many('res.partner', string="Partners")
    is_published = fields.Boolean(track_visibility='onchange')

    @api.multi
    def _compute_website_url(self):
        super(MentorshipForum, self)._compute_website_url()
        for forum in self:
            forum.website_url = "/mentorship/%s" % slug(forum)

    # @api.multi
    # def website_publish_button(self):
    #     self.ensure_one()
    #     return self.website_publish_button()

    def request_button(self):
        self.state = 'waiting_for_approval'

    def approve_button(self):
        self.state = 'approved'

    def reject_button(self):
        self.state = 'reject'

    def cancel_button(self):
        self.state = 'cancel'
