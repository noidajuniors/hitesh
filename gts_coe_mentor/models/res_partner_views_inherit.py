from odoo import api, fields, models, _
from odoo.exceptions import UserError, Warning, ValidationError
from datetime import timedelta, date, datetime
import calendar


class CoeMentorLine(models.Model):
    _name = 'coe.mentor.line'
    _inherit = 'mail.thread'
    _description = 'COE Mentor'

    coe_id = fields.Many2one('coe.setup.master',string="COE")
    mentor_id = fields.Many2one('res.partner',string="Mentor", domain=[('coe_partner_type', '=', 'mentor')])
    category_name_id = fields.Many2one('mentor.category', string='Mentorship Category')
    job_position = fields.Char('Job Position')
    email = fields.Char('Personal Email')
    mobile = fields.Char("Mobile")


    @api.onchange('mentor_id')
    def onchange_mentor(self):
        if self.mentor_id:
            self.category_name_id= self.mentor_id.category_name_id.id
            self.job_position= self.mentor_id.job_position
            self.email= self.mentor_id.email
            self.mobile= self.mentor_id.mobile if self.mentor_id.mobile else self.mentor_id.phone







class CoeSetup(models.Model):
    _inherit = 'coe.setup.master'

    mentor_line_ids = fields.One2many('res.partner', 'coe_id', string='Mentors')
    mentor_ids = fields.Many2many('res.partner', 'rel_mentor_coe', 'mentor_id', 'coe_id', string="Mentors")

    coe_mentor_line_ids = fields.One2many('coe.mentor.line', 'coe_id',  string="Mentors")


class ConsultantType(models.Model):
    _name = 'consultant.type'
    _description = 'Consultant Type'

    name = fields.Char("Name")
    desc = fields.Char("Description")


class AssignLines(models.TransientModel):
    _name = 'assign.lines'

    partner_id = fields.Many2one('res.partner')
    sr_no = fields.Char('Sr.No')
    access_id_no = fields.Char('Access Id No')

class IncubationFunding(models.Model):
    _name = "incubation.funding"

    incubator_accelerator_id = fields.Many2one("res.partner")
    incubator_accelerator_name = fields.Char("Incubator/Accelerator name")
    program_name = fields.Char("Program Name")
    services_availed=fields.Char("Services Availed")
    funding_received=fields.Integer("Funding Received")
    date_from=fields.Date("From Date")
    date_to=fields.Date("To Date")

class InovatationInfo(models.Model):
    _name = "inovatation.info"

    invotation_id = fields.Many2one("res.partner")
    ip_name =fields.Char("IP Name")
    ip_description=fields.Char(" IP Description")
    patent_description =fields.Char("Patent Description")
    patent_type = fields.Selection([
        ('design', 'Design'),
        ('utility', 'Utility'),
        ('plant', 'Plant'),
        ('provisional', 'Provisional')], string='Patent Type', default='design')


class ResPartner(models.Model):
    _name = "res.partner"
    _inherit = ['mail.thread', 'mail.activity.mixin','res.partner']
    _description = 'Onboarding Request'

    def _get_coe_id(self):
        user = self.env.user
        if user.coe_partner_type == 'coe':
            coe_id = self.env['coe.setup.master'].search([('partner_id', '=', user.entity_id.id)])
            if not coe_id:
                coe_id = self.env['coe.setup.master'].search([('partner_id', '=', user.partner_id.id)])
            return coe_id.id or False

    uom_id = fields.Many2one('uom.uom', "Uom")
    product_uom_id = fields.Many2one('product.uom', 'Unit of Measure', ondelete='cascade')

    unit_price = fields.Float("Price")
    tax_ids = fields.Many2many('account.tax', string="Taxes")
    is_mentor = fields.Boolean('Is a Mentor')
    signup_type = fields.Selection([('fellows', 'Fellows'),
                                    ('associations', 'Associations'),
                                    ('reset', 'Reset'),
                                    ('academia', 'Academia')], string="SignUp Type")
    coe_partner_type = fields.Selection([('mentor', 'Mentor'),
                                         ('investor', 'Investor'),
                                         ('incubatee', 'Incubatee'),
                                         ('consultant', 'Consultant'),
                                         ('alumini', 'Alumini'),
                                         ('partner', 'Partner'),
                                         ('coe', 'COE'),
                                         ('center', 'Center'),
                                         ('directorate', 'Directorate'),
                                         ('hq', 'HQ'),
                                         ('portfolio', 'Portfolio'),
                                         ('academia', 'Academia'),
                                         ('member', 'Member')], string="COE Type")
    is_incubatee = fields.Boolean('Is a Incubatee')
    mentor_domain_id = fields.Many2one('mentor.domain', string="Mentor Domain")
    document_ids = fields.One2many('upload.document', 'partner_id', 'Upload Documents')
    is_investor = fields.Boolean('Is Investor')
    is_consultant = fields.Boolean('Is Consultant')
    stpi_loc = fields.Many2one('res.branch', string='STPI Location')
    coe = fields.Many2one('coe.setup.master', string='COE(Deprecated)')
    about_coe = fields.Many2one('coe.details', string='How did you come to know about COE')
    type_of_company = fields.Selection([
        ('Pvt_ltd', 'Pvt ltd'),
        ('Partnership', 'Partnership'),
        ('proprietor', 'Proprietor'),
        ('yet_to_be_formed', 'Yet to be Formed')], string ="Company Structure")
    period_type = fields.Selection([('hrs', 'Hrs'),
                                    ('day', 'Days'),
                                    ('month', 'Months'), ], string="Period Type")
    partner_view_type = fields.Selection([('implementation', 'Impementation Partner'),
                                          ('academic', 'Academic Partner'),
                                          ('technical', 'Technical Partner'),
                                          ('funding', 'Funding Partner'),
                                          ('knowledge', 'Knowledge Partner'),
                                          ('industry', 'Industry Partner'),
                                          ], string="Partner Type")
    max_service_dur = fields.Float('Maximum Service Duration')
    min_service_dur = fields.Float('Minimum Service Duration')
    gender = fields.Selection([('Male', 'Male'),
                               ('Female', 'Female'),
                               ('Other', 'Other')], string='Gender')
    nationality = fields.Many2one('res.country', string='Nationality')
    brief = fields.Char('Brief about Team')
    category_name_id = fields.Many2one('mentor.category', string='Mentorship Category')
    describe_company = fields.Text('Describe about Company Product/Solution in brief')
    unique_inno = fields.Text('What is unique/innovative about your idea')
    academy = fields.Char('Name of Academy/Institute')
    company_id = fields.Many2one('res.partner', string='Company')
    religion_id = fields.Many2one('religion.type', string='Religion')
    availability_mentorship = fields.Text('Availability for Mentorship')
    support = fields.Char('Please tell us what support would you expect from CoE')
    synopsis_profile = fields.Text('Synopsis of Profile')
    mentor_comment = fields.Text('Mentor Comment')
    mentor_expertise_ids = fields.One2many('mentor.expertise', 'mentor_partner_id', string='Expertise')
    qualification_id = fields.Many2many('qualification.details', string='Qualification')
    qualification_ids = fields.One2many('qualification.details', 'mentor_id', string='Qualification')
    job_position = fields.Char('Job Position')
    email2 = fields.Char('Personal Email')
    cin = fields.Char('CIN')
    dept_detail = fields.Char('Debt. Details (If Any)')
    equity = fields.Char('Equity Details (If Any)')
    team_size = fields.Integer('Founding Team Size')
    funds = fields.Char('Fund Invested/Intend to Invest by Founders')
    experience_year = fields.Integer('Years of relevant experience')
    management_line_ids = fields.One2many('management.team', 'management_id', string='Management Team')
    employer_line_ids = fields.One2many('experience.lines', 'employee_child', string='Experience Details')
    founder_line_ids = fields.One2many('founder.details', 'founder_id', string='Founder Details')
    director_line_ids = fields.One2many('director.details', 'director_id', string='Director Details')
    service_ids = fields.Many2many('product.product', 'service_offer', 'service_id', 'res_id', string='Services')
    states = fields.Selection([
        ('draft', 'Draft'),
        ('wip', 'Waiting For Approval'),
        ('onbording', 'On Boarding'),
        ('onborded', 'On Boarded'),
        ('Approved', 'Approved'),
        ('left', 'Left'),
        ('cancel', 'Cancelled'), ], string='State',
        copy=False, default='draft', track_visibility='onchange')

    state = fields.Selection([
        ('draft', 'Draft'),
        ('onbording', 'On Boarding'),
        ('onborded', 'On Boarded'),
        ('waiting', 'Waiting For Approval'),
        ('approved', 'Approved'),
        ('left', 'Left'),
        ('cancel', 'Cancel'),
    ], string='Status',
        copy=False, default='draft',
        track_visibility='onchange')

    type = fields.Char(string='Type')
    seq_id = fields.Char('Sequence')
    communication_type = fields.Selection([
        ('business', 'Business Address'),
        ('home', 'Home Address'),
        ('email', 'Email')], string='Communications',
        copy=False, track_visibility='onchange')
    communication_opportunities = fields.Selection([
        ('representative', 'Representative'),
        ('accountant', ' Accountant'),
        ('other', 'Other'),
        ('only_me', ' Only send to me')], string='Communications of new opportunities ',
        copy=False, track_visibility='onchange')
    company_name = fields.Char(string='Company Name')
    ivestment_ids = fields.Many2many('investment.master', 'investor_id', 'investment_id',
                                     string='Past Investment')
    amount_individual = fields.Selection([
        ('lessthan50k', ' Less than $ 50,000'),
        ('fifty_to_hundred', ' $50,000 - $100,000'),
        ('hundred_to_two', ' $100,001 - $200,000'),
        ('morethan_two', ' More than $200,000')], string='The amount of individual income I received in 2017 was: ',
        copy=False, track_visibility='onchange')
    potential_investment_amount = fields.Selection([
        ('25', ' $25,000'),
        ('25_50', ' $25,001 - $50,000'),
        ('50_100', '$50,001 - $100,000'),
        ('100_200', ' $100,001 - $200,000'),
        ('more_200', 'More than $200,000')], default='25', string='Potential Investment Budget', copy=False)
    description_comapny = fields.Text('Description')
    company_state_id = fields.Many2one('res.country.state')
    company_zip = fields.Char('ZIP')
    company_country_id = fields.Many2one('res.country', string='Country')
    company_street = fields.Char('Street')
    company_street2 = fields.Char('Street2')
    company_city = fields.Char('City')
    company_email = fields.Char('Email')
    company_telephone = fields.Char('Telephone Number')
    room_floor_id = fields.Many2one('room.room', string="Room")
    domain_id = fields.Many2one('domain.master', 'Domain')
    sub_domain_id = fields.Many2one('domain.master', string="Sub Domain")
    tech_ids = fields.Many2many('domain.technology', 'rel_technology_d', 'technology_c', 'technology_s',
                                string="Technology")
    expertise_id = fields.Many2one('mentor.expertise', string="Expertise")
    r_and_r = fields.Text("R&R")
    deliverable = fields.Char("Deliverables")
    service_incubatee_ids = fields.One2many('coe.service.location', 'incubatee_id', string="Service Request")
    invoice_id = fields.Many2one('account.invoice', string="Invoice Number")
    invoice_date = fields.Date("Date Of Invoice")
    professiona_background = fields.Many2one('professional.background', 'Professional Background')
    name_of_acedmic = fields.Char("Name of Academy/Institution")
    profile_details = fields.Char("Profile Details")
    designation_id = fields.Many2one('hr.job', "Designation/Position ")
    upload_uom = fields.Binary('Upload MOU *')
    desc_mou = fields.Char('Partnership Agreement')
    upload_lou = fields.Binary('Upload LOU *')
    desc_lou = fields.Char('Description LOU *')
    file_name = fields.Char("Name")
    social_fb = fields.Char("Facebook")
    social_linkedin = fields.Char("Linked In")
    social_twitter = fields.Char("Twitter")
    details = fields.Char("Details")
    coe_ids = fields.Many2many('coe.setup.master', 'rel_coe_setup_master', 'parent_coe', 'child_coe', string="COE")
    incubatee_ids = fields.Many2many('res.partner', 'rel_res_parter_ids',
                                     'parent_partner', 'child_partner', string="Incubatee",
                                     domain=[('coe_partner_type', '=', 'incubatee')])
    nature_of_profession = fields.Selection([('acedmic', 'Academic'),
                                             ('non_acedmic', 'Non Academic')], string="Nature of Profession")
    name_of_institude = fields.Char("Name Of Instutaion")
    name_of_company = fields.Char("Company/Corporation/Trust")
    gstin_no = fields.Char("GST")
    pan_no = fields.Char("PAN")
    tan_no = fields.Char("TAN")
    aadhar_no = fields.Char("Aadhar")
    partner_type_data = fields.Many2one('partner.type', string="Investor Type")
    startup_stage = fields.Char("Preferred Startup Stages")
    focus_indust = fields.Char("Focus Industries")
    focus_sector = fields.Char("Focus Sector(s)")
    preffered_startup = fields.Char("Preferred Startup Stages")
    service_loc = fields.Many2one('coe.service.location', string='Service Location')
    service_area = fields.Char("Service Area")
    review_rating = fields.Text("Review and Rating")
    portfolio_ids = fields.One2many('portfolio.master', 'partner_id', "Portfolio")
    achevied_scroe = fields.Float("Achieved Score")
    meeting_count = fields.Integer("Meeting Count")
    season_details = fields.Char("Season Details")
    contact_mobile = fields.Char("Mobile")
    contact_landline = fields.Char("Landline")
    # contact_person = fields.Many2one('res.partner', "Contact person")
    contact_person = fields.Char("Contact person")

    contact_person_department = fields.Many2one('hr.job', "Contact person designation")
    application_no = fields.Char("Application season Number")
    date_from = fields.Date("Date From")
    date_to = fields.Date("Date To")
    screen_process_id = fields.Char("Screening Process ID")
    upload_id = fields.Binary("upload final business proposal/presentation")
    screen_data = fields.Float("Screening Process Final Score")
    screen_process = fields.Float("Screening Process Result")
    on_date_from = fields.Date("target timeline for onsite Start Date")
    on_date_to = fields.Date("target timeline for onsite End Date")
    comp_date = fields.Date("Target date for completion of onboarded incubatee profile")
    approval_date = fields.Date("Target date for approval of incubatee profile")
    portfolie_id = fields.Many2one('portfolio.master', string='Prtfolio Manager')
    coe_id = fields.Many2one('coe.setup.master', "COE", default=_get_coe_id)
    incubatee_space_ids = fields.One2many('incubatee.space', 'incubatee_id', 'Incubatee Space Assign')
    service_location_ids = fields.One2many('coe.service.location', 'service_req_id', string="Service location")
    bouquet_service_ids = fields.One2many('bouquet.line', 'bouquet_id', string="Bouquet Service")
    credit_service_line = fields.One2many('credit.service.line', 'partner_id', string="Cedit Limit Service")
    on_boarding_bool = fields.Boolean('On-Boarding', default=False)
    web_signup = fields.Boolean('Web Enrollment', default=False)
    manager_id = fields.Many2one('res.partner', 'Portfolio Manager')
    hr_manager = fields.Many2one('hr.employee', 'Portfolio Manager')
    card_no = fields.Char('Access ID')
    profile_status = fields.Selection([('data', 'Data Entered'),
                                       ('assigned', 'Onboarding(Program Assigned)'),
                                       ('portfolio', 'Onboarding(Assigned Portfolio Mgr)'),
                                       ('info', 'Onboarding(Profile Pending)'),
                                       ('profile', 'Onboarding(Profile Pending)'),
                                       ('profile_approved', 'Onboarding(Profile Information Apporved)'),
                                       ('equity', 'Onboarding(Equity Purchased)'),
                                       ('space', 'Onboarding(Space Assigned)'),
                                       ('assets', 'Onboarding(Assets Allocated)'),
                                       ('physical', 'Onboarding(Physically On-boarded)'),
                                       ('exit', 'Exited'),
                                       ], readonly=True, track_visibility='onchange')

    strategic_mentor = fields.Many2one('res.partner', "Strategic Mentor")
    strategic_mentorship_line = fields.One2many('strategic.mentorship.history', 'strategic_id')
    # investment_line = fields.One2many('investment.request', 'incubatee_id')
    equity_percent = fields.Float(string='Equity%')
    total_share = fields.Float("Total Share")
    price_per_share = fields.Float("Price Per Share", track_visibility='onchange')
    share_value = fields.Float("Share Value", compute='_find_share_value', track_visibility='onchange', store=True)
    applicable_share = fields.Float("Distributed Share")
    created_by = fields.Many2one('res.users', "Created By")
    confirmed_by = fields.Many2one('res.users', "Confirmed By")
    # date = fields.Datetime("Created Date")
    equity_detail_bool = fields.Boolean("Detail Hide")
    date_of_onboarding = fields.Date("Date of Onboarding", )
    initiate_from = fields.Selection([
        ('incubatee_self', 'By Self'),
        ('terminate', 'By COE/Manager'), ], string='Initiate From', track_visibility='onchange', copy=False,
        default='incubatee_self')
    consultant_type = fields.Many2one('consultant.type', string="Consultant Type")
    background_details = fields.Text(string="Background Details")
    prod_service_detail = fields.Many2one('product.service.detail', string="Product/Service Name")
    use_case_ids = fields.Many2many('use.case', 'rel_use_case', 'case_child', 'case_sub',
                                    string="Use Case")
    card_line_ids = fields.One2many('assign.lines', 'partner_id', string='Card Lines')


    # ======================================================Applicant Fields=============================================
    applicant_signup = fields.Boolean("Applicant Signup")
    funded_type = fields.Selection([('funded', 'Funded'), ('bootstrapped', 'Bootstrapped')],
                                   string="Funded/Bootstrapped?")
    mentorship_type = fields.Selection([('specilist', 'Domain Specilist'), ('generalist', 'Generalist')],
                                       default='specilist', string="Mentorship Type")

    registered_with = fields.Selection([('sebi', 'Sebi'), ('tie', 'TIE'),('venture',"Let's Venture")],
                                       default='sebi', string="Registered With")

    website = fields.Char('Website')
    app_link = fields.Char('Mobile App Link')
    organizer = fields.Char("Institute Name")
    active_year = fields.Float("Number Of Years Active")
    industry_id = fields.Many2one('domain.master', string="Sub Domain")
    sector_id = fields.Many2one('domain.master', string="Domain", )
    stage_id = fields.Many2one('startup.stages', string="Startup Stages")
    entity_type = fields.Selection([('privete', 'Private Limited Company'),
                                    ('partnership', 'Limited Liability Partnership'),
                                    ('one_person', 'One Person Company'),
                                    ('registered', 'Registered Partnership'),
                                    ('llc', 'LLC'),
                                    ('other', 'Other'),
                                    ], string="Nature Of Entity")
    applicant_industry_ids = fields.Many2many('domain.master', string="Sub Domain")
    applicant_sector_ids = fields.Many2many('domain.master', string="Domain", )
    startup_stages_id = fields.Many2many('startup.stages', string="Preferred Stages")
    interested_id = fields.Many2many('interested.details', string='Interested')
    services_ids = fields.Many2many('services.details', string="Services")
    institution_type = fields.Many2many('institution.type', string="Institution Type")
    affliat_type = fields.Selection([('affliated', 'Affliated'), ('deemed', 'Deemed')], default='affliated',
                                    string="Affliat Type")
    affliat_detail = fields.Char("Affliat")
    success_story_line = fields.One2many('success.story.line', 'partner_id', string="Success Story")
    success_bool = fields.Boolean("Success")
    degree_avl = fields.Many2many('degree.available', string="Degree Available")

    # ====================================================== New Fields============================================
    tech_segment = fields.Many2many('domain.technology', string="Tech Segment")

    problem_info = fields.Text('Problem Statement')
    innovation = fields.Text("innovation/Solution Developed")
    pilots_outcomes = fields.Text("Pilots/Outcomes")
    ground_development = fields.Text("Ground Development")
    market_opportunity = fields.Text("Market Opportunity")
    scalability_idea = fields.Text("Scalability of the idea")
    competitive_landscape = fields.Text("Competitive Landscape")
    target_customer_segment = fields.Text("Target Customer Segment")
    market_positioning = fields.Text("Market Positioning")
    distribution_channel = fields.Text("Distribution Channel")
    partner_network = fields.Text("Partner Network (If Appllicable)")
    sales_revenue = fields.Text("Sales/Revenue Model")
    cost_structure = fields.Text("Cost Structure")

    financial_investment_founders = fields.Text("Financial Investment by founders")
    external_funding_received = fields.Text("External funding Received")
    aggregate_generated = fields.Text("Aggregate Revenue Generated")

    expansion_plan = fields.Text("Expansion Plan")
    funding_requirement = fields.Text("Funding Requirement")
    year_founded =fields.Date("Year Founded")
    team_strength =fields.Integer("Team Strength")
    incubated_detail=fields.One2many("incubation.funding","incubator_accelerator_id","Incubated Detail")
    patents_line_ids = fields.One2many('inovatation.info', 'invotation_id', string="Patents")
    ip_line_ids = fields.One2many('inovatation.info', 'invotation_id', string="IP")
    label=fields.Char("Label",default = "Follwoing Fields inside General Infomation are Mandatory: Startup Name,Registered address,Year founded,Technology(ies) of Focus,Tech Segment,Company Structure,Founding Team Size ,Team Strength ")
    @api.multi
    def action_incubatee_showcase(self):
        shoecase_ids = None
        shoecase_ids = self.env['incubatee.showcase'].search([('incubatee_id', '=', self.id)])
        action = self.env.ref('gts_coe_incubatee.action_incubatee_showcase').read()[0]
        if len(shoecase_ids) > 1:
            action['domain'] = [('id', 'in', shoecase_ids.ids)]
        elif len(shoecase_ids) == 1:
            action['views'] = [(self.env.ref('gts_coe_incubatee.view_showcase_form').id, 'form')]
            action['res_id'] = shoecase_ids.ids[0]
        else:
            action['domain'] = [('id', 'in', shoecase_ids.ids)]
        action['context'] = {'create': True, 'edit': True}
        return action

    def update_record_action(self):
        if self.states not in ('left', 'cancel'):
            if self.states == 'Approved':
                self.states = 'wip'

    @api.model
    def create(self, vals):
        res = super(ResPartner, self).create(vals)
        # ***name_get****
        self.is_company = True
        self.company_type = 'company'
        return res

    @api.multi
    def schedule_meeting(self):
        return

    @api.multi
    def action_assign_manager(self):
        ctx = dict()
        ctx.update({'default_coe_id': self.coe_id.id, })
        return {
            'name': _('Portfolio'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wiz.portfolio.manager',
            'view_id': self.env.ref('gts_coe_mentor.wiz_portfolio_assign_view').id,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': ctx, }

    @api.multi
    @api.depends('applicable_share', 'price_per_share')
    def _find_share_value(self):
        for s in self:
            s.share_value = s.applicable_share * s.price_per_share

    @api.multi
    def button_submit(self):
        self.sudo().state = 'waiting'

    @api.multi
    def button_onboard(self):
        self.write({'state': 'onbording', 'profile_status': 'data'})

    def infra_view(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/seat-map',
            'target': 'new',
            'name': 'Infrastructure Seat Map View'}

    @api.multi
    def button_signup(self):

        user_list=[]
        user_id = self.env['res.users'].with_context(partner_id=self.id).sudo().create(
            {'login': self.email, 'name': self.name,
             'email': self.email, 'partner_type': 'incubatee', 'partner_id': self.id})

        group_id = self.env['res.groups'].sudo().search(
            [('name', '=', 'Incubatee'), ('category_id.name', '=', 'COE Extra Rights')])
        if user_id and group_id:
            self._cr.execute("Insert into res_groups_users_rel (gid,uid) Values(%s,%s)", (group_id.id, user_id.id))


        if user_id:
            user_id.action_id = self.env['ir.actions.actions'].search([('name', '=', 'My Control Panel')]).id
            user_id.partner_id.coe_partner_type = 'incubatee'
            try:
                user_id.action_reset_password()
            except Exception:
                pass
            template_id = self.env.ref('gts_coe_management.mail_template_for_incubatee_details')
            action_id = self.env.ref('gts_coe_mentor.update_profile_action_menu_view').id
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            params = "/web?debug=#id=%s&action=%s&model=res.partner&view_type=form" % (
                user_id.partner_id.id, action_id
            )
            purchase_url = str(base_url) + str(params)
            if template_id:
                values = template_id.generate_email(self.id)
                values['email_to'] = self.email
                values['email_from'] = self.env.user.partner_id.email
                values['body_html'] = values['body_html'].replace('_purchase_url', purchase_url)
                mail = self.env['mail.mail'].create(values)
                try:
                    mail.send()
                except Exception:
                    pass

        # ============ To Create Service Activity  ==============
        activity_type = self.env['mail.activity.type'].search([('is_notify_type', '=', True)])
        res_user_ids = self.env['res.users'].search(['|',('partner_id.parent_id', '=', self.coe_id.partner_id.id),('entity_id', '=', self.coe_id.partner_id.id)])
        if not res_user_ids:
            res_user_ids = self.env['res.users'].search([('partner_id', '=', self.coe_id.partner_id.parent_id.id)])
        for user in res_user_ids:
            user_list.append(user)
        if not activity_type:
            raise UserError(_("Please Configure Activity Type."))
        for user_id in user_list:
            activity_id = self.env['mail.activity'].create({
                'res_model': 'res.partner',
                'res_model_id': self.env.ref('gts_coe_mentor.model_res_partner').id,
                'res_id': self.id,
                'activity_type_id': activity_type.id,
                'user_id': user_id.id,
                'summary':"Onboarding Request-" + str(self.seq_id) + "From-" + str(self.name)
            })

        self.profile_status = 'profile'

    @api.multi
    def assign_space(self):
        space_ids = self.env['incubatee.space'].search([('incubatee_id', '=', self.id)])
        res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_incubatee', 'action_incubatee_space')
        res['domain'] = [('id', 'in', space_ids.ids)]
        res['context'] = {'default_incubatee_id': self.id,
                          'default_coe_id': self.coe_id.id}
        return res

    @api.multi
    def assign_asset_allocation(self):
        assets_ids = self.env['asset.assign'].search([('incubatee_id', '=', self.id)])
        res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_management', 'action_asset_assign_view')
        res['domain'] = [('id', 'in', assets_ids.ids)]
        res['context'] = {'default_incubatee_id': self.id, 'default_coe_id': self.coe_id.id}
        return res

    @api.multi
    def assign_program(self):
        res = {}
        # user = self.env.user
        prog_id = self.env['program.assign'].search([('incubatee_id', '=', self.id)])
        res = self.env['ir.actions.act_window'].for_xml_id('coe_program_management', 'action_program_assign_id')
        res['domain'] = [('id', 'in', prog_id.ids)]
        res['context'] = {'default_incubatee_id': self.id, 'default_program_type_id': 'primary'}
        # if user.partner_type == 'coe':
        #     res['context'] = {'default_coe_admin_bool': True}
        return res

    @api.model
    def create(self, vals):
        res = super(ResPartner, self).create(vals)
        if not res.coe_partner_type == 'mentor':
            if res.coe_id:
                res.parent_id = res.coe_id.partner_id.id
        return res

    @api.multi
    def write(self, vals):
        res = super(ResPartner, self).write(vals)
        if not vals.get('coe_partner_type') == 'mentor':
            if vals.get('coe_id'):
                self.parent_id = self.coe_id.partner_id.id
        return res

    @api.multi
    def action_equity_evaluation(self):
        eve_ids = self.env['equity.evaluation'].search([('incubatee_id', '=', self.id)]) or []
        action = self.env.ref('gts_coe_mentor.equity_evaluation_action_view_id1').read()[0]
        if len(eve_ids) > 1:
            action['domain'] = [('id', 'in', eve_ids.ids)]
        elif len(eve_ids) == 1:
            action['views'] = [(self.env.ref('gts_coe_mentor.equity_evaluation_form_view_id').id, 'form')]
            action['res_id'] = eve_ids.ids[0]
        else:
            action['domain'] = [('id', 'in', [])]
        return action

    @api.multi
    def confirm_button(self):
        # ============ To Create Service Activity  ==============
        user_list=[]
        activity_type = self.env['mail.activity.type'].search([('is_notify_type', '=', True)])
        res_user_ids = self.env['res.users'].search(['|', ('partner_id.parent_id', '=', self.coe_id.partner_id.id),
                                      ('entity_id', '=', self.coe_id.partner_id.id)])
        # res_user_ids = self.env['res.users'].search([('entity_id', '=', self.coe_id.partner_id.parent_id.id)])
        if not res_user_ids:
            res_user_ids = self.env['res.users'].search([('partner_id', '=', self.coe_id.partner_id.parent_id.id)])
        coe_user = self.env['res.users'].search([('partner_id', '=', self.coe_id.partner_id)])
        if res_user_ids:
            for user in res_user_ids:
                user_list.append(user)
        user_list.append(coe_user)
        if not activity_type:
            raise UserError(_("Please Configure Activity Type."))
        for user_id in user_list:
            activity_id = self.env['mail.activity'].create({
                'res_model': 'res.partner',
                'res_model_id': self.env.ref('gts_coe_mentor.model_res_partner').id,
                'res_id': self.id,
                'activity_type_id': activity_type.id,
                'user_id': user_id.id,
            })
        self.states = 'wip'

    # ======***Equity Action**=======
    @api.multi
    def action_equity(self):
        res = {}
        equity_id = self.env['equity.detail'].search([('incubatee_id', '=', self.id)])
        res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'equity_detail_action_view_id')
        res['domain'] = [('id', 'in', equity_id.ids)]
        res['context'] = {'default_incubatee_id': self.id, 'equity_percent': self.coe_id.equity_percent}
        return res

    # ========*Equity sale History Action**==========
    @api.multi
    def sale_history_action(self):
        res = {}
        equity_sale_ids = self.env['equity.sale'].search([('incubatee_id', '=', self.id),
                                                          ('state', 'in', ('payment_process', 'paid'))])
        res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'equity_sales_action_view')
        res['domain'] = [('id', 'in', equity_sale_ids.ids)]
        res['context'] = {'create': False, 'edit': False, 'delete': False}

        return res

    # ==========*Equity Purchase History Action**====
    @api.multi
    def purchase_history_action(self):
        res = {}
        equity_id = self.env['equity.detail'].search(
            [('incubatee_id', '=', self.id), ('state', 'in', ('payment', 'paid')), ])
        res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'equity_tree_action_view_id')
        res['domain'] = [('id', 'in', equity_id.ids)]
        res['context'] = {'create': False, 'edit': False, 'delete': False}
        return res

    @api.multi
    def action_initiate_exit_button(self):
        res = {}
        exit_ids = self.env['exit.form'].search([('incubatee_id', '=', self.id)])
        res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_incubatee', 'action_request_for_self')
        res['domain'] = [('id', 'in', exit_ids.ids)]
        res['context'] = {'default_incubatee_id': self.id, 'default_date_of_onboarding': self.date_of_onboarding,
                          'default_initiate_from': 'incubatee_self'}
        return res

    def action_add_alumini_button(self):
        res = {}
        alumini_ids = self.env['coe.alumini'].search([('incubatee_id', '=', self.id)])
        res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_coe_alumini')
        res['domain'] = [('id', 'in', alumini_ids.ids)]
        res['context'] = {'default_incubatee_id': self.id,
                          'default_date_of_onboarding': self.date_of_onboarding,
                          'default_street': self.street,
                          'default_street2': self.street2,
                          'default_city_id': self.city_id.id,
                          'default_state_id': self.state_id.id,
                          'default_zip': self.zip,
                          'default_country_id': self.country_id.id,
                          'default_coe': self.coe.id,
                          'default_company_id': self.company_id.id,
                          'default_gstin_no': self.gstin_no,
                          'default_tan_no': self.tan_no,
                          'default_experience_year': self.experience_year,
                          'default_brief': self.brief,
                          'default_email': self.email,
                          'default_image': self.image,
                          'default_type_of_company': self.type_of_company,
                          }
        return res

    @api.multi
    def approve_button_onboard(self):
        # ============Activity delete==============
        activities = self.env['mail.activity'].search([('id', 'in', self.activity_ids.ids)])
        for activity in activities:
            activity.unlink()
        user_id = self.env['res.users'].create({'name': self.name, 'login': self.email, 'email': self.email})
        if user_id:
            user_id.partner_id.coe_partner_type = 'incubatee'
            user_id.partner_id.email = self.email
            user_id.partner_id.email2 = self.email
            try:
                user_id.action_reset_password()
            except Exception:
                pass
            template_id = self.env.ref('gts_coe_management.mail_template_for_incubatee_details')
            action_id = self.env.ref('gts_coe_mentor.update_profile_action_menu_view').id
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            params = "/web?debug=#id=%s&action=%s&model=res.partner&view_type=form" % (user_id.partner_id.id, action_id)
            purchase_url = str(base_url) + str(params)
            if template_id:
                values = template_id.generate_email(self.id)
                values['email_to'] = self.email
                values['email_from'] = self.env.user.partner_id.email
                values['body_html'] = values['body_html'].replace('_purchase_url', purchase_url)
                mail = self.env['mail.mail'].create(values)
                try:
                    mail.send()
                except Exception:
                    pass
            self.state = 'created'

    @api.multi
    def cancel_button(self):
        self.states = 'cancel'

    @api.multi
    def approve_button(self):
        account_id = self.env['account.account'].search([('name', '=', 'Local Sales')], limit=1)
        self.profile_status = 'profile_approved'
        self.states = 'Approved'
        for res in self:
            if res.coe_partner_type == 'incubatee':
                res.seq_id = self.env['ir.sequence'].next_by_code('incubatee.default.seq')
                user = self.env['res.users'].search([('partner_id', '=', res.id)])
                user.action_id = self.env['ir.actions.actions'].search([('name', '=', 'My Control Panel')]).id
                if not user:
                    user_id = self.env['res.users'].with_context(partner_id=self.id).sudo().create(
                        {'login': self.email, 'name': self.name, 'email': self.email, 'partner_type': 'incubatee',
                         'partner_id': self.id})
                    user_id.action_id = self.env['ir.actions.actions'].search([('name', '=', 'My Control Panel')]).id
                    group_id = self.env['res.groups'].sudo().search(
                        [('name', '=', 'Incubatee'), ('category_id.name', '=', 'COE Extra Rights')])
                    if user_id and group_id:
                        self._cr.execute("Insert into res_groups_users_rel (gid,uid) Values(%s,%s)",
                                         (group_id.id, user_id.id))
                res.states = 'Approved'
            if res.coe_partner_type == 'mentor':
                res.seq_id = self.env['ir.sequence'].next_by_code('metor.default.seq')
                user_id = self.env['res.users'].with_context(partner_id=self.id).sudo().create(
                    {'login': self.email, 'name': self.name, 'email': self.email, 'partner_type': 'mentor',
                     'partner_id': self.id})
                group_id = self.env['res.groups'].sudo().search([('name', '=', 'Coe Mentor')])
                user_id.action_id=self.env['ir.actions.actions'].search([('name', '=', 'My Control Panel')]).id
                if user_id and group_id:
                    self._cr.execute("Insert into res_groups_users_rel (gid,uid) Values(%s,%s)",
                                     (group_id.id, user_id.id))
                if user_id:
                    user_id.partner_id.coe_partner_type = 'mentor'
                    try:
                        user_id.action_reset_password()
                    except Exception:
                        pass
                    template_id = self.env.ref('gts_coe_management.mail_template_for_mentor_details')
                    action_id = self.env.ref('gts_coe_mentor.res_partner_action_menu_view').id
                    base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
                    params = "/web?debug=#id=%s&action=%s&model=res.partner&view_type=form" % (
                        user_id.partner_id.id, action_id)
                    purchase_url = str(base_url) + str(params)
                    if template_id:
                        values = template_id.generate_email(self.id)
                        values['email_to'] = self.email
                        values['email_from'] = self.env.user.partner_id.email
                        values['body_html'] = values['body_html'].replace('_purchase_url', purchase_url)
                        mail = self.env['mail.mail'].create(values)
                        try:
                            mail.send()
                        except Exception:
                            pass

                    res.states = 'Approved'

            if res.coe_partner_type == 'investor':
                res.seq_id = self.env['ir.sequence'].next_by_code('investor.default.seq')
                user_id = self.env['res.users'].with_context(partner_id=self.id).sudo().create(
                    {'login': self.company_email, 'name': self.name, 'email': self.company_email,
                     'partner_type': 'investor', 'partner_id': self.id})
                group_id = self.env['res.groups'].sudo().search([('name', '=', 'Coe Investor')])
                user_id.action_id = self.env['ir.actions.actions'].search([('name', '=', 'My Control Panel')]).id
                if user_id and group_id:
                    self._cr.execute("Insert into res_groups_users_rel (gid,uid) Values(%s,%s)",
                                     (group_id.id, user_id.id))
                if user_id:
                    user_id.partner_id.coe_partner_type = 'investor'
                    try:
                        user_id.action_reset_password()
                    except Exception:
                        pass
                    template_id = self.env.ref('gts_coe_management.mail_template_for_investor_details')
                    action_id = self.env.ref('gts_coe_mentor.action_investor').id
                    base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
                    params = "/web?debug=#id=%s&action=%s&model=res.partner&view_type=form" % (
                        user_id.partner_id.id, action_id)
                    purchase_url = str(base_url) + str(params)
                    if template_id:
                        values = template_id.generate_email(self.id)
                        values['email_to'] = self.email
                        values['email_from'] = self.env.user.partner_id.email
                        values['body_html'] = values['body_html'].replace('_purchase_url', purchase_url)
                        mail = self.env['mail.mail'].create(values)
                        try:
                            mail.send()
                        except Exception:
                            pass
                res.states = 'Approved'
            if res.coe_partner_type == 'consultant':
                res.seq_id = self.env['ir.sequence'].next_by_code('consultant.default.seq')
                res.states = 'Approved'
            if res.coe_partner_type == 'member':
                res.seq_id = self.env['ir.sequence'].next_by_code('member.default.seq')
                res.states = 'Approved'
            if res.coe_partner_type == 'partner':
                res.seq_id = self.env['ir.sequence'].next_by_code('partner.default.seq')
                res.states = 'Approved'

            if res.service_incubatee_ids:
                list_data = []
                for line in res.service_incubatee_ids:
                    dic = {
                        'product_id': line.product_id.id,
                        'quantity': line.qty,
                        'price_unit': line.price,
                        'name': line.product_id.product_tmpl_id.name,
                        'account_id': account_id.id
                    }
                    list_data.append((0, 0, dic))
                invoice_id = self.env['account.invoice'].create({'partner_id': res.id, 'invoice_line_ids': list_data})
                if invoice_id:
                    res.invoice_id = invoice_id.id
                    res.invoice_date = datetime.datetime.today().date()

    @api.model
    def create_invoice_for_partner(self):
        partner_ids = self.env['res.partner'].search(
            [('coe_partner_type', '=', 'incubatee'), ('states', '=', 'Approved')])
        dict = {}
        list_data = []
        current_date = datetime.datetime.today().date()
        account_id = self.env['account.account'].search([('name', '=', 'Local Sales')], limit=1)
        for data in partner_ids:
            if data.invoice_date:
                if data.invoice_date.month < current_date.month:
                    if data.service_incubatee_ids:

                        for line in data.service_incubatee_ids:
                            dic = {
                                'product_id': line.product_id.id,
                                'quantity': line.qty,
                                'price_unit': line.price,
                                'name': line.product_id.product_tmpl_id.name,
                                'account_id': account_id.id
                            }
                            list_data.append((0, 0, dic))
            elif data.service_incubatee_ids:
                list_data = []
                for line in data.service_incubatee_ids:
                    dic = {
                        'product_id': line.product_id.id,
                        'quantity': line.qty,
                        'price_unit': line.price,
                        'name': line.product_id.product_tmpl_id.name,
                        'account_id': account_id.id
                    }
                    list_data.append((0, 0, dic))
            invoice_id = self.env['account.invoice'].create(
                {'partner_id': data.id, 'invoice_line_ids': list_data})
            if invoice_id:
                data.invoice_id = invoice_id.id
                data.invoice_date = current_date

    @api.multi
    def open_member(self):
        res = {}
        user = self.env['res.users'].search([('id', '=', self._uid)])
        if user.partner_type == 'hq' or user.partner_type == 'directorate':
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_member_new1')
            res['domain'] = [('coe_partner_type', '=', 'member')]
        else:
            # res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_member_new1_only')
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_member_new1')
            res['context'] = {'create': False, 'edit': True, 'delete': False, }
            res['domain'] = [('coe_partner_type', '=', 'member')]
        return res

    @api.multi
    def open_partner(self):
        res = {}
        user = self.env['res.users'].search([('id', '=', self._uid)])
        if user.partner_type == 'hq' or user.partner_type == 'directorate':
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_member_partner')
            res['domain'] = [('coe_partner_type', '=', 'partner')]
        else:
            # res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_partner_new1_only')
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_member_partner')
            res['context'] = {'create': False, 'edit': True, 'delete': False, }
            res['domain'] = [('coe_partner_type', '=', 'partner')]
        return res

    @api.multi
    def open_consultant(self):
        res = {}
        user = self.env['res.users'].search([('id', '=', self._uid)])
        if user.partner_type == 'hq' or user.partner_type == 'directorate':
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_consultant')
            res['domain'] = [('coe_partner_type', '=', 'consultant')]
        else:
            # res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_consultant_new1_only')
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_consultant')
            res['context'] = {'create': False, 'edit': True, 'delete': False, }
            res['domain'] = [('coe_partner_type', '=', 'consultant')]
        return res

    @api.multi
    def open_my_incubatee(self):
        all_incubatee = self.env['res.partner'].search([('strategic_mentor', '=', self.env.user.partner_id.id)])
        action = self.env.ref('gts_coe_mentor.update_profile_action_menu_view').read()[0]
        if len(all_incubatee) > 1:
            action['domain'] = [('id', 'in', all_incubatee.ids)]
        elif len(all_incubatee) == 1:
            action['views'] = [(self.env.ref('gts_coe_mentor.new_res_partner_incubatee_view').id, 'form')]
            action['res_id'] = all_incubatee.ids[0]
        else:
            action['domain'] = [('id', 'in', all_incubatee.ids)]
        action['context'] = {'create': False, 'edit': True}
        return action

    @api.multi
    def open_profile(self):
        res = {}
        partner_id = None
        coe_id = None
        user = self.env['res.users'].search([('id', '=', self._uid)])
        if user.partner_type == False and user.applicent_signup == True:
            applicant_id = self.env['applicant.signup'].search(
                [('partner_type', '=', user.partner_type), ('partner_id', '=', user.partner_id.id)])
            res = self.env['ir.actions.act_window'].for_xml_id('coe_auth_signup', 'action_applicant_signup')
            if applicant_id:
                res['res_id'] = applicant_id.id
                res['views'] = [(self.env['ir.ui.view'].get_view_id('gts_coe_management.view_coe_setup_form'), 'form')]
            else:
                res['domain'] = [('partner_id', '=', user.partner_id.id)]
                res['views'] = [(self.env['ir.ui.view'].get_view_id('gts_coe_management.view_coe_setup_form'), 'form')]

        elif user.partner_type == 'incubatee':
            partner_id = self.env['res.partner'].search(
                [('coe_partner_type', '=', 'incubatee'), ('id', '=', user.partner_id.id)])
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'update_profile_action_menu_view')
            # form_view_id = self.env.ref('gts_coe_mentor.new_res_partner_incubatee_view')
            if partner_id:
                res['res_id'] = partner_id.id
                res['domain'] = [('id', '=', partner_id.id)]
                res['views'] = [(self.env['ir.ui.view'].get_view_id('gts_coe_mentor.new_res_partner_incubatee_view'), 'form')]
            else:
                res['domain'] = [('coe_partner_type', '=', 'incubatee'), ('id', '=', user.partner_id.id)]
                res['views'] = [
                    (self.env['ir.ui.view'].get_view_id('gts_coe_mentor.new_res_partner_incubatee_view'), 'form')]


        elif user.partner_type == 'investor':
            partner_id = self.env['res.partner'].search(
                [('coe_partner_type', '=', 'investor'), ('id', '=', user.partner_id.id)])
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_investor')
            if partner_id:
                res['views'] = [(self.env['ir.ui.view'].get_view_id('gts_coe_mentor.res_partner_investor_form'), 'form')]
                res['res_id'] = partner_id.id
            else:
                res['domain'] = [('coe_partner_type', '=', 'investor'), ('id', '=', user.partner_id.id)]
                res['views'] = [
                    (self.env['ir.ui.view'].get_view_id('gts_coe_mentor.res_partner_investor_form'), 'form')]

        elif user.partner_type == 'mentor':
            partner_id = self.env['res.partner'].search(
                [('coe_partner_type', '=', 'mentor'), ('id', '=', user.partner_id.id)])
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'res_partner_action_menu_view')
            if partner_id:
                res['views'] = [(self.env['ir.ui.view'].get_view_id('gts_coe_mentor.new_res_partner_form_view_mentor'), 'form')]
                res['res_id'] = partner_id.id
            else:
                res['domain'] = [('coe_partner_type', '=', 'mentor'), ('id', '=', user.partner_id.id)]
                res['views'] = [
                    (self.env['ir.ui.view'].get_view_id('gts_coe_mentor.new_res_partner_form_view_mentor'), 'form')]


        elif user.partner_type == 'consultant':
            partner_id = self.env['res.partner'].search(
                [('coe_partner_type', '=', 'consultant'), ('id', '=', user.partner_id.id)])
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_consultant')
            if partner_id:
                res['views'] = [(self.env['ir.ui.view'].get_view_id('gts_coe_mentor.res_partner_consultant_form'), 'form')]
                res['res_id'] = partner_id.id
            else:
                res['domain'] = [('coe_partner_type', '=', 'consultant'), ('id', '=', user.partner_id.id)]
                res['views'] = [
                    (self.env['ir.ui.view'].get_view_id('gts_coe_mentor.res_partner_consultant_form'), 'form')]


        elif user.partner_type == 'member':
            partner_id = self.env['res.partner'].search(
                [('coe_partner_type', '=', 'member'), ('id', '=', user.partner_id.id)])
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_member_new1')
            if partner_id:
                res['views'] = [(self.env['ir.ui.view'].get_view_id('gts_coe_mentor.res_member_form'), 'form')]
                res['res_id'] = partner_id.id
            else:
                res['domain'] = [('coe_partner_type', '=', 'member'), ('id', '=', user.partner_id.id)]
                res['views'] = [(self.env['ir.ui.view'].get_view_id('gts_coe_mentor.res_member_form'), 'form')]


        elif user.partner_type == 'partner':
            partner_id = self.env['res.partner'].search(
                [('coe_partner_type', '=', 'partner'), ('id', '=', user.partner_id.id)])
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_member_partner')
            if partner_id:
                res['views'] = [(self.env['ir.ui.view'].get_view_id('gts_coe_mentor.res_partner_member_partner_form'), 'form')]
                res['res_id'] = partner_id.id
            else:
                res['domain'] = [('coe_partner_type', '=', 'partner'), ('id', '=', user.partner_id.id)]
                res['views'] = [
                    (self.env['ir.ui.view'].get_view_id('gts_coe_mentor.res_partner_member_partner_form'), 'form')]


        elif user.partner_type == 'coe':
            coe_id = self.env['coe.setup.master'].search(['|',('partner_id', '=', user.partner_id.id),('partner_id', '=', user.entity_id.id)])
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_management', 'action_coe_setup_master')
            if coe_id:
                res['res_id'] = coe_id.id
                res['views'] = [(self.env['ir.ui.view'].get_view_id('gts_coe_management.view_coe_setup_form'), 'form')]
            else:
                res['domain'] = [('coe_partner_type', '=', 'coe'), ('id', '=', user.partner_id.id)]
                res['views'] = [(self.env['ir.ui.view'].get_view_id('gts_coe_management.view_coe_setup_form'), 'form')]

        else:
            res = self.env['ir.actions.act_window'].for_xml_id('gts_coe_mentor', 'action_member_partner')
            res['domain'] = [('id', '=', user.partner_id.id)]

        res['context'] = {'create': False, 'edit': True}
        return res


class SuccessSstoryLine(models.Model):
    _inherit = "success.story.line"
    _description = 'Success Story Line'

    partner_id = fields.Many2one('res.partner')


class ManagementTeam(models.Model):
    _name = 'management.team'

    management_id = fields.Many2one('res.partner')
    name = fields.Char('Name')
    job_position = fields.Many2one('hr.job', 'Job Position')
    years_in_company = fields.Integer('Years in Company')
    brief_company = fields.Char('Brief Description about Background')


class Experience(models.Model):
    _name = 'experience.lines'

    employee_child = fields.Many2one('res.partner')
    employer = fields.Char('Employer')
    leave_reason = fields.Char('Reason for Leaving')
    from_date = fields.Date('From')
    to_date = fields.Date('To')
    job_position = fields.Many2one('hr.job', 'Job Position')
    service_pertiod_month = fields.Integer("Service Period(Month)")
    service_pertiod_year = fields.Integer("Service Period(Year)")
    document = fields.Binary("Document")


class FounderDetails(models.Model):
    _name = 'founder.details'

    founder_id = fields.Many2one('res.partner')
    details_type = fields.Selection([
        ('founder', 'Founder'),
        ('director', 'Director')
    ], string='Detail Type')
    name = fields.Char('Name')
    director_number = fields.Char('Director Identification Number')
    state_id = fields.Many2one('res.country.state')
    zip = fields.Char('ZIP')
    country_id = fields.Many2one('res.country', string='Country')
    street = fields.Char('Address Line 1')
    street2 = fields.Char('Address Line 2')
    city_id = fields.Many2one('res.city', 'City')


class DirectorDetails(models.Model):
    _name = 'director.details'

    director_id = fields.Many2one('res.partner')
    details_type = fields.Selection([
        ('founder', 'Founder'),
        ('director', 'Director')
    ], string='Detail Type')
    name = fields.Char('Name')
    director_number = fields.Char('Director Identification Number')
    state_id = fields.Many2one('res.country.state')
    zip = fields.Char('ZIP')
    country_id = fields.Many2one('res.country', string='Country')
    street = fields.Char('Address Line 1')
    street2 = fields.Char('Address Line 2')
    city_id = fields.Many2one('res.city', 'City')


class DocumentationDetails(models.Model):
    _name = "documentation.details"

    document_id = fields.Many2one('res.partner')
    document_type = fields.Many2one('document.type', 'Document Type')
    attach_cards = fields.Binary('Attach Document', compute='_attach_card', inverse='_card_name', copy=False)
    attach_cards_name = fields.Char('Cards Name')
    attachment_id = fields.Many2one('ir.attachment')
    description = fields.Char('Description')

    @api.multi
    @api.depends('attach_cards')
    def _attach_card(self):
        for every in self:
            val = every.attachment_id.datas
            every.attach_cards = val

    @api.multi
    def _card_name(self):
        Attachment = self.env['ir.attachment']
        for each in self:
            attachment_value = {
                'name': each.attach_cards_name or '',
                'datas': each.attach_cards or '',
                'datas_fname': each.attach_cards_name or '',
                'type': 'binary',
                'res_model': "res.partner",
                'res_id': each.document_id.id,
                'document_type': each.document_type.id,
            }
            attachment = Attachment.sudo().create(attachment_value)
            each.attachment_id = attachment.id
            return attachment_value


class MentorDomain(models.Model):
    _name = 'mentor.domain'

    name = fields.Char("Name")


class PartnerType(models.Model):
    _name = 'partner.type'

    name = fields.Char("Name")


class PortfolioMater(models.Model):
    _name = 'portfolio.master'

    partner_id = fields.Many2one('res.partner', "Partner")
    name = fields.Char("Name")
    description = fields.Char("Description")
    website = fields.Char("Website")
    email = fields.Char("Website")
    domain_id = fields.Many2one('domain.master', 'Sector')


class ResUsers(models.Model):
    _inherit = 'res.users'

    location_type = fields.Selection([('directorate', 'Directorate'),
                                      ('center', 'Center'),
                                      ('hq', 'HQ')], string='Location Type')
    user_location = fields.Many2one('res.branch', string='User Location')
    applicent_signup = fields.Boolean("Web Signup")
    partner_type = fields.Selection([('mentor', 'Mentor'),
                                     ('investor', 'Investor'),
                                     ('incubatee', 'Incubatee'),
                                     ('consultant', 'Consultant'),
                                     ('alumini', 'Alumini'),
                                     ('partner', 'Partner'),
                                     ('coe', 'COE'),
                                     ('center', 'Center'),
                                     ('directorate', 'Directorate'),
                                     ('hq', 'HQ'),
                                     ('portfolio', 'Portfolio'),
                                     ('academia', 'Academia'),
                                     ('member', 'Member'),
                                     ('hrms', 'HRMS')], string="Users Type")
    entity_id = fields.Many2one('res.partner', string="Entity")

    @api.onchange('partner_type')
    def partner_type_onchage(self):
        self.entity_id = None

    @api.model
    def create(self, vals):
        res = super(ResUsers, self).create(vals)
        if self.entity_id:
            res.partner_id.parent_id = res.entity_id.parent_id.id
        return res

    @api.multi
    def write(self, vals):
        res = super(ResUsers, self).write(vals)
        if vals.get('entity_id'):
            self.partner_id.parent_id = self.entity_id.parent_id.id
        return res


class StrategicMentorshipHistory(models.Model):
    _name = 'strategic.mentorship.history'
    description = "Strategic Mentorship History"

    strategic_id = fields.Many2one('res.partner')
    mentor_request_id = fields.Many2one('mentorship.request')
    mentor_id = fields.Many2one('res.partner', string="Mentor Name")
    category_name_id = fields.Many2one('mentor.category', string='Mentor Category')
    start_date = fields.Datetime("Mentorship Duration Start")
    end_date = fields.Datetime("Mentorship Duration End")


class BouquetServiceLine(models.Model):
    _name = 'bouquet.line'
    _description = "Bouquet Service Management"

    company_id = fields.Many2one('res.company', string='Company', track_visibility='always', copy=False,
                                 default=lambda self: self.env.user.company_id.id)
    currency_id = fields.Many2one('res.currency', related='company_id.currency_id', readonly=True)
    service_bouquet_id = fields.Many2one('product.bundle', string='Service Bouquet')
    bouquet_id = fields.Many2one('res.partner', string='Requester')
    total_price = fields.Float("Bouquet Cost", )
    tax_ids = fields.Many2many('account.tax', string="Taxes")
    package_type = fields.Selection([('foc', 'FOC'), ('paid', 'Paid')], string='Package Type')
    discount_category_id = fields.Many2one('discount.category', string="Discount Category")
    total_amount = fields.Float("Total Amount", )
    periodicity = fields.Many2one('periodicity.master', 'Periodicity')
    effective_date = fields.Date("Bouquet Start Date")
    expire_date = fields.Date("Bouquet End Date")
    cancel_date = fields.Date("Bouquet End Date")
    service_state = fields.Selection([('active', 'Active'), ('cancel', 'Cancelled'), ('expire', 'Expired')],
                                     string='Status', copy=False, track_visibility='onchange')
    package_lines = fields.One2many('package.bundle.line', 'bundle_package_id', string="Services", )


class PackageBundleLine(models.Model):
    _name = "package.bundle.line"

    package_id = fields.Many2one('product.bundle', string='Package Bouquet')
    bundle_package_id = fields.Many2one('bouquet.line', string='Service')
    service_loc_id = fields.Many2one('location.service', string="Service", )
    uom_id = fields.Many2one('uom.uom', 'Unit of Measure')
    product_uom_id = fields.Many2one('product.uom', 'Unit of Measure', ondelete='cascade')

    quantity = fields.Integer("Quantity")


class CreditServiceLine(models.Model):
    _name = 'credit.service.line'
    _description = "Credit Service Line"

    # ================compute Credit limit Consumption on monthly basis===========
    @api.one
    def finnd_credit_limit_consumption(self):
        if self.type != 'bouquet_service':
            today_date = datetime.today().date()
            year, month = today_date.year, today_date.month
            range = calendar.monthrange(year, month)[1]
            first_day = date(year, month, 1)
            last_day = date(year, month, range)
            first_day -= timedelta(days=1)
            last_day += timedelta(days=1)
            total = 0
            request_lines = self.env['services.requests.line'].search([('request_id.requester_id', '=', self.partner_id.id),
                                                                       ('request_id.state', '=', 'approved'),
                                                                       ('partner_type', '=', 'incubatee'),
                                                                       ('request_type', '=', 'request_service'),
                                                                       ('perpare_service', '=', 'no'),
                                                                       ('from_date', '>=', first_day),
                                                                       ('to_date', '<=', last_day),
                                                                       ])
            for line in request_lines:
                total += line.actual_duration
            self.consumption = total

    partner_id = fields.Many2one('res.partner', string='Service')
    credit_limit_id = fields.Many2one('credit.limit', string='Credit Limit')
    credit_limit_line_id = fields.Many2one('credit.limit.line', string='Credit Limit')
    service_id = fields.Many2one('location.service', string='Service')
    uom_id = fields.Many2one('uom.uom', 'UOM', ondelete='cascade')
    product_uom_id = fields.Many2one('product.uom', 'Unit of Measure', ondelete='cascade')

    periodicity = fields.Many2one('periodicity.master', 'Periodicity')
    duration_type = fields.Selection([('hrs', 'Hrs'), ('day', 'Days')], default='hrs', string="Duration")
    credit_limit = fields.Float("Credit Limit")
    expiry_date = fields.Date("Credit Expiry Date")
    remark = fields.Char("Remark")
    credit_state = fields.Selection([('active', 'Active'), ('expire', 'Expired')], string='Status',
                                    related='credit_limit_line_id.credit_state', store=True,
                                    track_visibility='onchange')
    usage_service = fields.Selection(
        [('common_service', 'Common Services'), ('request_service', 'Request Basis Services')],
        default='common_service', string='Request Type')
    consumption = fields.Float("Credit Limit Consumption", compute='finnd_credit_limit_consumption')
    bouquet_consumption = fields.Float("Bouquet Consumption")
    type = fields.Selection([('credit_limit', 'Credit Limit'), ('bouquet_service', 'Bouquet Service')],  string="Type")
