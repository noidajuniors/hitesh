from odoo import api, fields, models, _
from odoo.exceptions import UserError, Warning, ValidationError
from datetime import datetime, time


class IncubateServiceMaster(models.Model):
    _name = 'incubate.service.master'
    _rec_name = "setup_coe_id"
    _inherit = ['mail.thread']

    product_id = fields.Many2one('product.product', string="Service Name")
    service_location_id = fields.Many2one('coe.service.location', string="Service Location")
    setup_coe_id = fields.Many2one('coe.setup.master', string="COE")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('approve', 'Active'),
        ('cancel', 'Expired')], string='Status', default='draft')
    sale_price = fields.Float("Sale Price")
    quantity = fields.Float("Quantity")
    service_id = fields.Char("Service Id")

    price_derivation = fields.Char("Price Derivation Rationale")
    max_sale_allocation = fields.Char("Max Allowed Sale Margin for COE")
    periodicity = fields.Many2one('Periodicity')
    category_id = fields.Many2one('product.category', 'Service Category', readonly=True)
    effective_date = fields.Date("Price Effective Date")
    payment_term_id = fields.Many2one('account.payment.term', string="Payment Term")
    discount_category_id = fields.Many2one('discount.category', string="Discount Category")
    available_qty = fields.Float("Available Qty")
    request_qty = fields.Float("Request Qty")
    approved_qty = fields.Float("Approved Qty")
    discount_percentage = fields.Float(string='Discount')
    discount_amount = fields.Float(string='Discount Amount')
    quantity = fields.Float(string='Avaliable in Quantity')
    image_medium = fields.Binary('Medium', compute="_get_image", store=False, attachment=True)
    data_type = fields.Selection([('coe', "Coe"), ('incubatee', "Incubatee")], string="Coe Type")

    @api.multi
    def request_approval(self):
        self.state = 'approve'

    @api.multi
    def cancel_button(self):
        self.state = 'cancel'

    @api.multi
    def action_set_to_draft(self):
        self.state = 'draft'

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        """
        Overrides orm field_view_get.
        @return: Dictionary of Fields, arch and toolbar.
        """

        res = super(IncubateServiceMaster, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
                                                                 submenu=submenu)
        contect_data = self.env.context.get('default_data_type')
        list_data = []
        if view_type == "tree":
            doc = res['toolbar']['action']
            for data in doc:
                if contect_data == 'incubatee' and data['xml_id'] == 'gts_coe_management.act_server_incubatee':
                    list_data.append(data)
                elif contect_data == 'coe' and data['xml_id'] == 'gts_coe_management.act_server_alumini':
                    list_data.append(data)
        if list_data:
            res['toolbar'].update({'action': list_data})

        return res

    @api.onchange('product_id')
    def _onchange_categ(self):
        for rec in self:
            rec.category_id = rec.product_id.categ_id

    @api.depends('product_id')
    def _get_image(self):
        for record in self:
            if record.product_id.image_medium:
                record.image_medium = record.product_id.image_medium
            else:
                record.image_medium = False

    @api.onchange('discount_category_id', 'sale_price')
    def _onchange_sale_price(self):
        if self.sale_price and self.discount_category_id:
            if self.discount_category_id.discount_type == 'percentage':
                self.discount_amount = self.sale_price - (self.sale_price * (self.discount_percentage / 100))
            else:
                self.discount_amount = self.sale_price - self.discount_amount

    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.product_id.category_ids:
            domain = [('id', 'in', self.product_id.category_ids.ids)]
            return {'domain': {'discount_category_id': tuple(domain)}}


class ServiceRequestCoe(models.Model):
    _name = 'service.request.coe'
    _rec_name = 'name'
    _inherit = ['mail.thread']
    _order = "id desc"

    select_service = fields.Many2one('product.product', string='Service')
    service_loc = fields.Many2one('coe.service.location', string='Service Location')
    requested_by = fields.Many2one('res.users', string='Requested by', default=lambda self: self.env.user)
    added_by = fields.Many2one('res.users', string='Added By', default=lambda self: self.env.user)
    description = fields.Char('Description')
    service_category = fields.Many2one('product.category', 'Service Category')
    on_hand_qty = fields.Float('Available Quantity')
    price = fields.Float('Price')
    qty_required = fields.Float('Required Quantity')
    # coe_id = fields.Many2one('res.partner', domain=[('coe_partner_type', '=', 'coe')])
    # directorate_id = fields.Many2one('stpi.location', string='Center')
    directorate_id = fields.Many2one('res.branch', string='Center')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting_for_approval', 'Waiting for Approval'),
        ('approved', 'Approved'),
        ('cancel', 'Cancel'),
        ('reject', 'Rejected')
    ], string='Status', copy=False, default='draft', track_visibility='onchange')

    coe_location_ids = fields.One2many('coe.service.location', 'loc_request_id', string="Service location")

    name = fields.Char('Reference', copy=False, default=lambda self: _(''), readonly=True)

    requested_time = fields.Datetime("Requested Date", default=fields.Datetime.now, )
    approved_time = fields.Datetime("Approved Date")
    cancel_remarks = fields.Char('Cancel Remarks')
    reject_remarks = fields.Char('Reject Remarks')

    @api.multi
    def unlink(self):
        if self.state == 'approved':
            raise UserError(_("You cannot delete this Request as it is in Approved State."))
        return super(ServiceRequestCoe, self).unlink()

    @api.onchange('select_service')
    def onchange_service(self):
        self.description = self.select_service.description
        self.on_hand_qty = self.select_service.qty_available
        self.price = self.select_service.lst_price
        self.service_category = self.select_service.categ_id.id

    @api.multi
    def request_button(self):
        if not self.coe_location_ids:
            raise Warning("Please Add Service Request Line!")
        if self.coe_location_ids:
            for line in self.coe_location_ids.filtered(lambda sl: sl.state in ('draft')):
                line.state = 'wip'
        self.state = 'waiting_for_approval'

    @api.multi
    def approve_button(self):
        self.state = 'approved'
        self.approved_time = fields.Datetime.now()
        if self.coe_location_ids:
            for line in self.coe_location_ids.filtered(lambda sl: sl.state in ('draft', 'wip')):
                line.state = 'approved'
        for record in self:
            record.state = 'approved'
            record_list = []
            loc = self.env['location.service']
            for line in record.coe_location_ids:
                loc.create({
                    'service_locate_id': self.directorate_id.id,
                    'product_id': line.product_id.id,
                    'available_qty': line.qty_available,
                    'request_qty': line.qty,
                    'sale_price': line.price,
                    'usage_service': line.product_id.usage_service,
                    'state': 'approved'
                })

    @api.multi
    def cancel_button(self):
        if not self.cancel_remarks:
            raise ValidationError(_("Please Enter Cancel Remarks!"))
        self.state = 'cancel'

    @api.multi
    def reject_button(self):
        if not self.reject_remarks:
            raise ValidationError(_("Please Enter Reject Remarks!"))
        self.state = 'reject'


class CoeRequestWizard(models.Model):
    _name = 'coe.request.wizard'

    # directorate_id = fields.Many2one('stpi.location', string='Center')
    directorate_id = fields.Many2one('res.branch', string='Center')

    coe_id = fields.Many2one('coe.setup.master', string='COE')
    coe_type = fields.Boolean(string="COE")
    incubatee_type = fields.Boolean(string="Incubatee")
    coe_wizard_ids = fields.One2many('coe.request.wizard.line', 'coe_wizrd_id', "Services")
    description = fields.Char('Description')

    @api.model
    def default_get(self, fields):
        res = super(CoeRequestWizard, self).default_get(fields)
        active_id = self._context.get('active_ids')
        list_line = []
        if active_id:
            for data in self.env['location.service'].browse(active_id):
                dic = {

                    'service_id': data.service_id,
                    'product_id': data.product_id.id,
                    # 'qty': data.request_qty,
                    'qty_available': data.available_qty,
                    'sale_price': data.sale_price

                }
                list_line.append((0, 0, dic))
            res.update({'coe_wizard_ids': list_line})

        # if self.env.user.partner_id.coe_partner_type == 'coe':
        #     res.update({'coe_id': self.env.user.partner_id.id})

        return res

    def add_service_request(self):
        active_id = self._context.get('active_ids')
        list_line = []
        # if active_id:
        for data in self.coe_wizard_ids:
            dic = {

                'product_id': data.product_id.id,
                'description': data.product_id.product_tmpl_id.name,
                'price': data.sale_price,
                'qty_available': data.qty_available,
                'qty': data.qty

            }
            list_line.append((0, 0, dic))
        # if self.coe_id:
        self.env['service.request'].create(
            {'coe_id': self.coe_id.id, 'description': self.description, 'service_location_ids': list_line})
        # else:
        self.env['service.request.coe'].create(
            {'directorate_id': self.directorate_id.id, 'description': self.description, 'coe_location_ids': list_line})


class CoeRequestWizardLine(models.Model):
    _name = 'coe.request.wizard.line'

    coe_wizrd_id = fields.Many2one('coe.request.wizard', "COE Wizard")
    service_id = fields.Char('Service ID')
    product_id = fields.Many2one('product.product', "Service")
    qty = fields.Float("Requested Qty.")
    qty_available = fields.Float("Available Qty")
    sale_price = fields.Float("Sales Price")


class ServiceRequestHq(models.Model):
    _name = 'service.request.hq'
    _rec_name = 'name'
    _inherit = ['mail.thread']
    _order = "id desc"

    product_id = fields.Many2one('product.product', string='Service')
    service_loc = fields.Many2one('coe.service.location', string='Service Location')
    requested_by = fields.Many2one('res.users', string='Requested by', default=lambda self: self.env.user)
    added_by = fields.Many2one('res.users', string='Approved By', default=lambda self: self.env.user)
    description = fields.Char('Description')
    service_category = fields.Many2one('product.category', 'Service Category')
    on_hand_qty = fields.Float('Available Quantity')
    price = fields.Float('Price')
    qty_required = fields.Float('Required Quantity')
    coe_id = fields.Many2one('res.partner', string='COE')
    # directorate_id = fields.Many2one('stpi.location', string='Directorate')
    directorate_id = fields.Many2one('res.branch', string='Directorate')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting_for_approval', 'Waiting for Approval'),
        ('approved', 'Approved'),
        ('cancel', 'Cancel'),
        ('reject', 'Rejected')
    ], string='Status', copy=False, default='draft', track_visibility='onchange')
    direct_location_ids = fields.One2many('service.request.line', 'hq_request_id', string="Requested Services")
    name = fields.Char('Reference', copy=False, default=lambda self: _(''), readonly=True)

    requested_time = fields.Datetime("Requested Date", default=fields.Datetime.now, )
    approved_time = fields.Datetime("Approved Date")
    cancel_remarks = fields.Char('Cancel Remarks')
    reject_remarks = fields.Char('Reject Remarks')

    @api.multi
    def unlink(self):
        if self.state == 'approved':
            raise UserError(_("You cannot delete this Request as it is in Approved State."))
        return super(ServiceRequestHq, self).unlink()

    @api.onchange('product_id')
    def onchange_service(self):
        self.description = self.product_id.description
        self.on_hand_qty = self.product_id.qty_available
        self.price = self.product_id.lst_price
        self.service_category = self.product_id.categ_id.id

    @api.multi
    def request_button(self):
        if self.name == _(''):
            self.name = self.env['ir.sequence'].next_by_code('directorate.service.sequence') or _('')
        if not self.direct_location_ids:
            raise Warning("Please Add Service Request Line!")
        if self.direct_location_ids:
            for line in self.direct_location_ids.filtered(lambda sl: sl.state in ('draft')):
                line.state = 'wip'
        self.state = 'waiting_for_approval'

    @api.multi
    def approve_button(self):
        self.state = 'approved'
        self.approved_time = fields.Datetime.now()
        if self.direct_location_ids:
            for line in self.direct_location_ids.filtered(lambda sl: sl.state in ('draft', 'wip')):
                line.state = 'approved'

        for record in self:
            record.state = 'approved'
            record_list = []
            loc = self.env['location.service']
            for line in record.direct_location_ids:
                loc.create({
                    'service_locate_id': self.directorate_id.id,
                    'product_id': line.product_id.id,
                    'available_qty': line.qty_available,
                    'request_qty': line.request_qty,
                    'approved_qty': line.approved_qty,
                    'sale_price': line.sale_price,
                    'usage_service': line.product_id.usage_service,
                    'state': 'approved'
                })
                # if line.product_id.perpare_service == 'yes':
                #     raise Warning("This is Prepaid Service please clear the due before assigning the request. ")

    @api.multi
    def cancel_button(self):
        if not self.cancel_remarks:
            raise ValidationError(_("Please Enter Cancel Remarks!"))
        self.state = 'cancel'

    # @api.multi
    # def create_invoice_button(self):
    #     view_ref = self.env[
    #         'ir.model.data'
    #     ].get_object_reference(
    #         'account', 'invoice_form'
    #     )
    #     view_id = view_ref[1] if view_ref else False
    #     if view_id:
    #         rec = {
    #             'type': 'ir.actions.act_window',
    #             'res_model': 'account.invoice',
    #             'view_type': 'form',
    #             'view_mode': 'form',
    #             'view_id': view_id,
    #             'target': 'current',
    #             'context': {
    #             }
    #         }
    # data_list = []
    # invoice_line = self.env['account.invoice']
    # context = self.env.context
    # print('context======================',context)
    # invooice = invoice_line.browse(context)
    # for line in self.direct_location_ids:
    #     rec = ({
    #         # 'inv_id': self.invoice_id.id,
    #         'product_id': line.product_id.id,
    #         # 'available_qty': line.qty_available,
    #         'quantity': line.request_qty,
    #         'price_unit': line.sale_price,
    #         # 'sale_price': line.sale_price,
    #     })
    #     data_list.append(0, 0, rec)
    # print(data_list, 'data_list')
    # invooice.create(data_list)
    # return rec
    # for record in self:
    #     print('pppppp')

    @api.multi
    def reject_button(self):
        if not self.reject_remarks:
            raise ValidationError(_("Please Enter Reject Remarks!"))
        self.state = 'reject'


class RequestLine(models.Model):
    _name = 'service.request.line'

    hq_request_id = fields.Many2one('service.request.hq', "HQ Request")
    product_id = fields.Many2one('product.product', 'Service')
    service_code = fields.Char('Service ID')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('wip', 'Waiting For Approval'),
        ('approved', 'Approved'),
        ('reject', 'Rejected'),
        ('cancel', 'Cancelled')], default='draft', string='Status', )
    sale_price = fields.Float("Sale Price")
    quantity = fields.Float("Quantity")
    periodicity = fields.Many2one('periodicity.master', 'Periodicity')
    qty_available = fields.Float("Available Qty")
    request_qty = fields.Float("Requested Qty", invisible=True)
    approved_qty = fields.Float("Approved Qty")
    uom_id = fields.Many2one('uom.uom', 'UOM')
    product_uom_id = fields.Many2one('product.uom', 'Unit of Measure', ondelete='cascade')

    discount_category = fields.Many2many('discount.category', 'service_discount_category_ref', 'category_id',
                                         'discount_category_id', string='Discount Category')

    @api.onchange('product_id')
    def onchange_product(self):
        for s in self:
            s.product_uom_id = s.product_id.product_uom_id.id
            s.discount_category = [(6, 0, s.product_id.category_ids.ids)]

    @api.multi
    def line_service_approve(self):
        for s in self:
            if s.state == 'draft' or s.state == 'wip':
                s.state = 'approved'

    @api.multi
    def line_service_cancel(self):
        for s in self:
            if s.state == 'draft' or s.state == 'wip':
                s.state = 'cancel'

    @api.multi
    def line_service_reject(self):
        for s in self:
            if s.state == 'draft' or s.state == 'wip':
                s.state = 'reject'


class HqRequestWizard(models.TransientModel):
    _name = 'hq.request.wizard'

    # product_id = fields.Many2one('product.product', string='HQ')
    # directorate_id = fields.Many2one('stpi.location', string='Directorate', default=lambda self: self.env.user.user_location.id)
    directorate_id = fields.Many2one('res.branch', string='Directorate',
                                     default=lambda self: self.env.user.user_location.id)
    # coe_id = fields.Many2one('res.partner', domain=[('coe_partner_type', '=', 'coe')])
    coe_type = fields.Boolean(string="COE")
    description = fields.Char(string='Description')
    incubatee_type = fields.Boolean(string="Incubatee")
    hq_wizard_ids = fields.One2many('hq.request.wizard.line', 'hq_wizrd_id', "Services")

    @api.model
    def default_get(self, fields):
        res = super(HqRequestWizard, self).default_get(fields)
        active_id = self._context.get('active_ids')
        list_line = []
        if active_id:
            for data in self.env['product.product'].browse(active_id):
                dic = {

                    'prod_id': data.id,
                    'service_id': data.default_code,
                    # 'qty': data.request_qty
                    # 'qty_available': data.no_of_unit,
                    'sale_price': data.lst_price

                }
                list_line.append((0, 0, dic))
            res.update({'hq_wizard_ids': list_line})

        # if self.env.user.partner_id.coe_partner_type == 'coe':
        #     res.update({'directorate_id': self.env.user.partner_id.id})

        return res

    def add_service_request(self):
        active_id = self._context.get('active_ids')
        list_line = []
        for data in self.hq_wizard_ids:
            dic = {

                'qty_available': data.qty_available,
                'product_id': data.prod_id.id,
                'sale_price': data.sale_price,
                'request_qty': data.qty

            }
            list_line.append((0, 0, dic))
        print(list_line, '+++++++++++++++')
        self.env['service.request.hq'].create(
            {'directorate_id': self.directorate_id.id, 'description': self.description,
             'direct_location_ids': list_line})


class HqRequestWizardLine(models.TransientModel):
    _name = 'hq.request.wizard.line'

    hq_wizrd_id = fields.Many2one('hq.request.wizard', "HQ Wizard")
    service_id = fields.Char("Service Id")
    prod_id = fields.Many2one('product.product', "Service")
    qty = fields.Float("Requested Qty.")
    qty_available = fields.Float("Available Qty")
    sale_price = fields.Float("Sales Price")


class ServiceLocation(models.Model):
    _inherit = 'coe.service.location'

    loc_request_id = fields.Many2one('service.request.coe', string='Service request')
    uom_id = fields.Many2one('uom.uom', 'UOM')
    product_uom_id = fields.Many2one('product.uom', 'Unit of Measure', ondelete='cascade')


    discount_category = fields.Many2many('discount.category', 'service_discount_category_ref', 'category_id',
                                         'discount_category_id', string='Discount Category')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('wip', 'Waiting For Approval'),
        ('approved', 'Approved'),
        ('reject', 'Rejected'),
        ('cancel', 'Cancelled')], string='Status', default='draft')

    @api.onchange('product_id')
    def onchange_product(self):
        for s in self:
            s.product_uom_id = s.product_id.product_uom_id.id
            s.discount_category = [(6, 0, s.product_id.category_ids.ids)]

    @api.multi
    def line_service_approve(self):
        for s in self:
            if s.state == 'draft' or s.state == 'wip':
                s.state = 'approved'

    @api.multi
    def line_service_cancel(self):
        for s in self:
            if s.state == 'draft' or s.state == 'wip':
                s.state = 'cancel'

    @api.multi
    def line_service_reject(self):
        for s in self:
            if s.state == 'draft' or s.state == 'wip':
                s.state = 'reject'
