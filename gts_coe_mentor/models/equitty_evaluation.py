from odoo import api, fields, models, _
from odoo.exceptions import UserError, Warning, ValidationError
from datetime import datetime, time


class EquityEvaluation(models.Model):
    _name = 'equity.evaluation'
    _inherit = ['mail.thread']
    _description = "Equity Evaluation"
    _order = 'start_date desc'
    _rec_name = 'name'

    READONLY_STATES = {
        'confirm': [('readonly', True)],
        'approve': [('readonly', True)], }

    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirm'),
        ('approve', 'Approved'),
    ], string='Status', default='draft')

    incubatee_id = fields.Many2one('res.partner', string="Incubatee", states=READONLY_STATES, )
    start_date = fields.Datetime("Start Date", states=READONLY_STATES)
    end_date = fields.Datetime("End Date", states=READONLY_STATES)
    evaluation_type = fields.Selection([
        ('merchant', 'Merchant Banker'),
        ('market', 'Market Evaluation'), ], string='Evaluation Type', required="1", default='merchant',
        states=READONLY_STATES)
    name = fields.Char('Reference', copy=False, default=lambda self: _('/'), readonly=True)
    total_share = fields.Float("Total Share", readonly="1")
    price_per_share = fields.Monetary("Price Per Share", readonly="1")
    equity_percent = fields.Float(string='Equity%', readonly="1")
    applicable_share = fields.Monetary("Applicable Share", readonly="1")
    share_value = fields.Monetary("Share Value", readonly="1")
    current_face_value = fields.Monetary("Current Face Value", states=READONLY_STATES)
    evaluation_date = fields.Datetime("Evaluation Date", states=READONLY_STATES)
    document_ids = fields.One2many('upload.document', 'equity_evl_id', 'Audit Report', states=READONLY_STATES)
    company_id = fields.Many2one('res.company', string='Company', track_visibility='always', copy=False,
                                 default=lambda self: self.env.user.company_id.id)
    currency_id = fields.Many2one('res.currency', related='company_id.currency_id', readonly=True)
    coe_id = fields.Many2one('coe.setup.master', related='incubatee_id.coe_id', store=True, readonly=1)

    @api.constrains('incubatee_id')
    @api.onchange('incubatee_id')
    def update_percent(self):
        equity_id = None
        equity_id = self.env['equity.detail'].search([('incubatee_id', '=', self.incubatee_id.id)], limit=1)
        if equity_id:
            self.equity_percent = equity_id.equity_percent
            self.total_share = equity_id.total_share
            self.price_per_share = equity_id.price_per_share
            self.equity_percent = equity_id.equity_percent
            self.share_value = equity_id.share_value
            self.applicable_share = equity_id.applicable_share

    @api.multi
    def action_confirm(self):
        self.name = self.env['ir.sequence'].next_by_code('equity.evaluation.sequence') or _('')
        self.state = 'confirm'

    @api.multi
    def action_approve(self):
        self.state = 'approve'
        if self.current_face_value > 0:
            self.incubatee_id.write({
                'price_per_share': self.current_face_value, })
