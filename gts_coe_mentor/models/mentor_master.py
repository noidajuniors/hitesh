from odoo import api, fields, models


class MentorCategory(models.Model):
    _name = "mentor.category"
    _custom = False
    _rec_name = "category_name"
    _inherit = ['mail.thread']

    category_name = fields.Char('Category')
    description = fields.Char('Description')
    strategic_bool = fields.Boolean("Strategic")


class MentorExpertiseName(models.Model):
    _name = "mentor.expertise.name"
    _custom = False
    _rec_name = "name"

    name = fields.Char('Name')
    description = fields.Char('Description')


class MentorExpertise(models.Model):
    _name = "mentor.expertise"
    _rec_name = "name"
    _inherit = ['mail.thread']

    name = fields.Char('Name')
    mentor_ex_id = fields.Many2one('mentor.expertise.name', 'Name')
    code = fields.Char('Code')
    expertise = fields.Char('Code')
    description = fields.Char('Description')
    mentor_partner_id = fields.Many2one('res.partner', string="Mentor Partner")


class Religion(models.Model):
    _name = "religion.type"
    _custom = False
    _rec_name = "religion"
    _inherit = ['mail.thread']

    religion = fields.Char('Religion')
    description = fields.Char('Description')


class QualificationDetails(models.Model):
    _name = "qualification.details"
    _custom = False
    _rec_name = "qualification_id"
    _inherit = ['mail.thread']

    qualification_id = fields.Many2one('qualification.master', string='Qualification')
    description = fields.Char('Description')
    mentor_id = fields.Many2one('res.partner', "Mentor")


class Qualification(models.Model):
    _name = "qualification.master"
    _custom = False
    _rec_name = "name"
    name = fields.Char("Qualification")
    description = fields.Char('Description')


class ProfessionalBackground(models.Model):
    _name = 'professional.background'
    _custom = False
    _inherit = ['mail.thread']

    name = fields.Char("Name")
