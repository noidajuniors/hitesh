from odoo import api, fields, models, _
from odoo.exceptions import UserError, Warning, ValidationError
from datetime import datetime, time


class MentorshipServiceDiscontinueWizard(models.Model):
    _name = 'mentorship.service.discontinue.wizard'

    mentorship_ids=fields.Many2many('mentorship.request', string="Discontinue Ids")
    remarks=fields.Char("Reason")

    @api.onchange('mentorship_ids')
    def find_diff_mentors(self):
        mentor_list = []
        for line in self.mentorship_ids:
            if line.partner_id.id not in mentor_list:
                mentor_list.append(line.partner_id.id)
            if line.state in ('draft', 'request', 'reject', 'reject'):
                raise UserError(_("Sorry! You can process Only with Approved Or Completed Requests."))
            if len(mentor_list) > 1:
                raise UserError(_("Sorry! You can Process Only with Single Mentor,Different Mentors not allowed at the same time."))

    @api.multi
    def confirm_discontinue_services(self):
        mentor_list=[]
        for line in self.mentorship_ids:
            if line.partner_id.id not in mentor_list:
                mentor_list.append(line.partner_id.id)
            if line.state  in ('draft','request','reject','reject'):
                raise UserError(_("Sorry! You can Request Only for Approved Or Completed Requests."))
            if len(mentor_list)>1:
                raise UserError(_("Sorry! You can select request only for one mentor,Different Mentors requests not allowed at the same time."))
        discontinue_id=self.env['mentorship.service.discontinue'].create({'remarks':self.remarks})
        for line in self.mentorship_ids:
            if line.discontinue_id and line.discontinue_id.state not in ('cancel','reject'):
                raise UserError(_("Sorry! You can not send Request-%s. %s -Service request already in progress.")% ((line.name,line.name)))
            line.write({'discontinue_id': discontinue_id.id})
            data=self.env['mentorship.service.discontinue.line'].create({''
                        'discontinue_id': discontinue_id.id,
                        'incubatee_id':line.incubatee_id.id,
                        'partner_id':line.partner_id.id,
                        'commencement_date':line.commencement_date,
                        'duration_type':line.duration_type,
                        'duration':line.duration,
                        'requested_date':line.requested_date,
                        'remarks':line.remarks,
                        'category_name_id':line.category_name_id.id,
                        'mentorship_request_id':line.id})
            discontinue_id.mentor_id =line.partner_id.id
        discontinue_id.request_button()

class CardLines(models.TransientModel):
    _name = 'card.lines'

    manager_id = fields.Many2one('wiz.portfolio.manager')
    sr_no = fields.Char('Sr.No', readonly=True)
    access_id_no = fields.Char('Access Id No')

class Manager(models.TransientModel):
    _name = 'wiz.portfolio.manager'

    partner_id=fields.Many2one('res.partner', string="Portfolio Manager")
    card_number = fields.Integer('Number of Cards')
    card_line_ids = fields.One2many('card.lines', 'manager_id', string='Card Lines')
    coe_id = fields.Many2one('coe.setup.master', "COE")
    hr_manager = fields.Many2one('hr.employee', 'Portfolio Manager')

    @api.onchange('card_number')
    def onchange_card_no(self):
        lines = [(5, 0, 0)]
        i = 1
        for rec in range(self.card_number):
            print('afasfs')
            if i <= self.card_number:
                dict={
                    'sr_no':i
                }
                lines.append((0,0, dict))
                i += 1
        self.card_line_ids = lines



    @api.multi
    def confirm_request(self):
        active_id = self._context.get('active_id',False)
        if active_id:
            partner = self.env['res.partner'].search([('id','=', active_id)])
            partner.write({'hr_manager': self.hr_manager.id, 'profile_status': 'portfolio'})
            partner.message_subscribe([self.partner_id.id])
        return True


    @api.multi
    def assign_card(self):
        active_id = self._context.get('active_id', False)
        if active_id:
            partner = self.env['res.partner'].search([('id', '=', active_id)])

            partner.write({'card_no': self.card_number,
                           'profile_status':'physical',
                           'state':'onborded'})
        lines = []
        for rec in self.card_line_ids:
            dict = {
                'sr_no': rec.sr_no,
                'access_id_no': rec.access_id_no
            }
            lines.append((0, 0, dict))
        partner.card_line_ids = lines


