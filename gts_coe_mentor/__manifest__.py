# -*- coding: utf-8 -*-
# Copyright 2015-2017 Geo Technosoft (<http://www.geotechnosoft.com>)

{
    'name': 'GTS COE Mentor',
    'summary': 'COE Mentor',
    'author': 'Geo Technosoft',
    'license': 'AGPL-3',
    'website': 'http://www.geotechnosoft.com',
    'category': '',
    'version': '1.0',
    'depends': ['event','contacts','project','hr','gts_coe_incubatee','gts_coe_management'],
    'data': [
        'wizard/mentorship_service_discontinue_wizard_view.xml',
        'security/ir.model.access.csv',
        'security/security_view.xml',
        'data/mentor_sequence.xml',
        'data/service_discontinue_sequence.xml',
        'data/mail_template.xml',
        'views/base_menu_planned.xml',
        'views/portfolio_master.xml',
        'views/mentor_partner_type.xml',
        'views/res_partner_form_inherit.xml',
        'views/mentor_domain_view.xml',
        'views/mentor_incubatee_mapping.xml',
        'views/incubatee_scedular_view.xml',
        'views/coe_service_data_view.xml',
        'views/mentorship_request_view.xml',
        'views/mentorship_forum_view.xml',
        'views/mentorship_service_discontinue_view.xml',
        'views/mentor_master_view.xml',
        "views/event_schedule_view.xml",
        "views/equity_detail_view.xml",
        "views/equitty_evaluation_view.xml",
        "views/equity_sale_view.xml",
        "views/coe_alumini_view.xml",
        "views/website_mentorship_layout.xml"
    ],

    'installable': True,
}
