from openerp import api, fields, models, tools, _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import datetime
import num2words
import decimal
from openerp.exceptions import UserError

from openerp.tools import amount_to_text_en


class ResPartner(models.Model):
    _inherit = "res.partner"

    balance_cutoff_date = fields.Date(string="Balance Cutoff Date")
    # amount_total = fields.Integer('Amount', required=True, default=0.0)
    due_amount = fields.Integer('Amount', compute="compute_due_amount", store=1)

    @api.multi
    def action_balance_send(self):
        '''
        This function opens a window to compose an email, with the edi sale template message loaded by default
        '''
        if not self.balance_cutoff_date:
            raise UserError(_('Please Enter Balance Cut Off Date !!'))
        self.ensure_one()
        # if not self.issued_total > 0.0:
        #     raise ValidationError('No Overdue Amount Remaining!')
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('gts_balance_confirmation_letter',
                                                             'email_template_balance_payment_send')[
                1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'res.partner',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    # @api.multi
    @api.depends('balance_cutoff_date')
    def compute_due_amount(self):
        for partner in self:
            if isinstance(partner.id, models.NewId):
                return 0
            else:
                if partner.balance_cutoff_date:
                    partner._cr.execute(
                        """select sum(debit-credit) from account_move_line line  
                        INNER JOIN account_move move on line.move_id = move.id 
                        where move.date < %s and line.partner_id =%s""", (partner.balance_cutoff_date, partner.id))
                    result = partner._cr.fetchone()[0]
                    if result is not None and result:
                        partner.due_amount = result
                    else:
                        partner.due_amount = 0
                    print(result, partner.due_amount)


    @api.multi
    def amount_to_text(self, amount, currency):
        convert_amount_in_words = amount_to_text_en.amount_to_text(amount, lang='en', currency='')
        convert_amount_in_words = convert_amount_in_words.replace(' and Zero Cent', ' Only ')
        return convert_amount_in_words

    def convert(self, amount, cur):
        from num2words import num2words
        if isinstance(amount, float):
            frac = str(amount).split('.')
            words = num2words(int(frac[0]), lang='en_IN')
            # if cur.name == 'USD':
            #     words = words + ' USD'
            if cur.name == 'INR':
                words = words + ' rupees'
            words = words.replace(' and', '')
            words = words.replace(',', '')
            word2 = ''
            if int(frac[1]) > 0:
                word2 = ' and ' + str(num2words(int(frac[1])))
                if cur.name == 'USD':
                    word2 = word2 + ' cents'
                elif cur.name == 'INR':
                    word2 = word2 + ' paisa'
            amt2txt = words + word2
        else:
            amt2txt = num2words(amount, lang='en_IN')
            amt2txt = amt2txt.replace(' and', '')
            amt2txt = amt2txt.replace(',', '')

        return amt2txt.title() + 'Only'
