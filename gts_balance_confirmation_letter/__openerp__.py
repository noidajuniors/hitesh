# -*- encoding: utf-8 -*-
##############################################################################
#
#
##############################################################################
{
    'name': "Balance Confirmation Report",
    'version': '1.0.0',
    'category': 'Report',
    'description': """
        
    """,
    'summary': '',
    'author': 'Geo Technosoft',
    'website': 'https://www.geotechnosoft.com',
    'depends': ['base','mail','account_reports'],
    'data': [
        # "views/res_partner_views.xml"
        "views/report_confirmation_letter.xml",
        "data/balance_confirmatation_template.xml"
    ],
    'installable': True,
    'application': True,
}

